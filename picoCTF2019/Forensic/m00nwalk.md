**Decode this [message](https://jupiter.challenges.picoctf.org/static/fc1edf07742e98a480c6aff7d2546107/message.wav) from the moon.**

```
$ wget https://jupiter.challenges.picoctf.org/static/fc1edf07742e98a480c6aff7d2546107/message.wav
$ file message.wav
message.wav: RIFF (little-endian) data, WAVE audio, Microsoft PCM, 16 bit, mono 48000 Hz
```
Wow we have an audio file! I'm expecting nothing, but I plug my headphones and try to listen something..ok, there's an unrecognizable sound.
We can start our challenge from the two hints suggested:

1. How did pictures from the moon landing get sent back to Earth?
2. What is the CMU mascot?, that might help select a RX option

Let's figure out the first one!

After some research, I've discovered that the transmission technique used by Apollo lunar mission was [SSTV](https://en.wikipedia.org/wiki/Slow-scan_television). This is a
method to transfer video or pictures over a voice bandwidth channel, which can make it practical to send video over thousands of miles via ionospheric propagation.

A good idea is to decode `message.wav` with a sstv tool in order to revert back the image related. I'd try it with [Slowrx](http://windytan.github.io/slowrx/).
Once opened there is a dropdown menu where you can choose the decoding mode..unfortunately there are dozens of chances!
Probably the second hint comes in help. If I google "CMU mascot" the first result points to **Scotty the Scottie Dog**, which is related to the *Scottie* modes suppoerted by slowrx.

Ok, so let's capture the sound produced by `message.wav` with `Scottie 1` encoding..wow I get this image!

![m00nwalk](./m00nwalk.png)

This image shows our flag, after another scanning I've recognized that the last string stands for "space".

Solution: `picoCTF{beep_boop_im_in_space}`
