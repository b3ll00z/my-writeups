**Find the flag in this [picture](https://jupiter.challenges.picoctf.org/static/916b07b4c87062c165ace1d3d31ef655/pico_img.png).**

```sh
$ wget https://jupiter.challenges.picoctf.org/static/916b07b4c87062c165ace1d3d31ef655/pico_img.png
$ file pico_img.png
pico_img.png: PNG image data, 600 x 600, 8-bit/color RGB, non-interlaced
```
This time we have a png image to inspect. Again, I'd try immediately if `strings` command succeed.

```sh
$ strings pico_img.png | grep 'picoCTF{'
picoCTF{s0_m3ta_d8944929}K
```
Wow, cool! This practice should be the first thing to do!