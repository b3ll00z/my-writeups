**This [garden](https://jupiter.challenges.picoctf.org/static/4153422e18d40363e7ffc7e15a108683/garden.jpg) contains more than it seems.**

```sh
$ wget https://jupiter.challenges.picoctf.org/static/4153422e18d40363e7ffc7e15a108683/garden.jpg
$ file garden.jpg
garden.jpg: JPEG image data, JFIF standard 1.01, resolution (DPI), density 72x72, segment length 16, baseline, precision 8, 2999x2249, components 3
```

Welcome to my first forensic writeup!
This first challange proposes a jpeg file to analyze with an hex editor, as suggested. But to go fast let's see if we can obtain something useful
immediately by `strings` command..

```sh
$ strings garden.jpg | grep 'picoCTF{'
Here is a flag "picoCTF{more_than_m33ts_the_3y33dd2eEF5}"
```
Perfect, flag captured :fireworks: