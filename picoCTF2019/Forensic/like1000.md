**This [.tar file](https://jupiter.challenges.picoctf.org/static/52084b5ad360b25f9af83933114324e0/1000.tar) got tarred a lot.**

```
$ tar xvf 1000.tar
999.tar
filler.txt
```

Mmmh, ok! I believe is a matrioska tar. To avoid to manually extract the archive 1000 times a programming approach is a survival thing :sweat_smile:

```
$ for i in $(seq -s' ' 1000 -1 1); do tar xvf $i.tar; done 
...
$ ls
... filler.txt flag.png
```

All right, now that we've reached the final tar archive let's open `flag.png`

![like1000](./like1000.png)

..and that's all folks!!!
