**This is a really weird text file [TXT](https://jupiter.challenges.picoctf.org/static/e7e5d188621ee705ceeb0452525412ef/flag.txt)? Can you find the flag?**

```sh
$ wget https://jupiter.challenges.picoctf.org/static/e7e5d188621ee705ceeb0452525412ef/flag.txt
$ file flag.txt
flag.txt: PNG image data, 1697 x 608, 8-bit/color RGB, non-interlaced
```
Uhm it sounds weird! A png file with a `.txt` extention. Obviously the scope of this challenge is intended to teach that an extension
appended at end of a file name doesn't reveal concretely the nature of the file itself.

Now, let's try again if `strings` wins.

```sh
$ strings flag.txt | grep 'picoCTF{'
$
```
Ok, nothing happens. Probably the flag is shown by the image itself. So a viewer may help us after having renamed the file.

```sh
$ mv flag.txt flag.png
$ viewnior flag.png
```

We obtain this image

![flag](./flag.png)

All right, flag captured! :beers: