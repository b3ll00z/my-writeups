**We found this [packet capture](https://jupiter.challenges.picoctf.org/static/b506393b6f9d53b94011df000c534759/capture.pcap). Recover the flag that was pilfered from the network.**

Another network forensic challenge! OK, let's open this packets snapshot with Wireshark.

A starting point is to follow the udp streams, as in the previous challenge. This may be helpful for our flag research.

Well, effectively the udp streams show a lot of content inherent of the flag format. For instance, the udp stream no.7 receives from `10.0.0.12` the content `icoCTF{StaT31355e`. I don't know if this host contains the flag or not, but I'm convinced that inspecting udp streams is the correct path.

Following the streams there's a lot of noisy strings but there's one the captures my attention. At `udp.stream eq 32` we have from `10.0.0.66` to `10.0.0.1` a message `"start"`.
From the same destination we also have a `"end"` message. Is there something between these two messages?

For a better understanding let's filter all streams on this specific destination with `ip.addr == 10.0.0.1` and then follow all its udp streams.
There is nothing to notify except one thing. Between the two strings `"start"` - `"end"` there is a content `5112 -> 22 Len=5` under **info** tab, for which some of the suffixes 5**112**, 5**105**, ... remind me ASCII letters.

So we can collect all of these printable characters in a string, which we hope is the hidden flag..

```
$ python
>>> flag = [112, 105, 99, 111, 67, 84, 70, 123, 112, 49, 76, 76, 102, 51, 114, 51, 100, 95, 100, 97, 116, 97, 95, 118, 49, 97, 95, 115, 116, 51, 103, 48, 125, 97]
>>> print("".join([chr(x) for x in flag]))
'picoCTF{p1LLf3r3d_data_v1a_st3g0}a'
```

Very good, flag captured!

Solution: `picoCTF{p1LLf3r3d_data_v1a_st3g0}`