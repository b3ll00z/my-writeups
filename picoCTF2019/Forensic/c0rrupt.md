**We found this [file](https://jupiter.challenges.picoctf.org/static/ab30fcb7d47364b4190a7d3d40edb551/mystery). Recover the flag.**

```
$ wget https://jupiter.challenges.picoctf.org/static/ab30fcb7d47364b4190a7d3d40edb551/mystery
$ file mystery
mystery: data
```

The hint of this challenge suggests to fix the file header. A quick look of the first bytes of `mystery` may reveal something useful.

```
$ xxd -g1 mystery
00000000: 89 65 4e 34 0d 0a b0 aa 00 00 00 0d 43 22 44 52  .eN4........C"DR
00000010: 00 00 06 6a 00 00 04 47 08 02 00 00 00 7c 8b ab  ...j...G.....|..
00000020: 78 00 00 00 01 73 52 47 42 00 ae ce 1c e9 00 00  x....sRGB.......
00000030: 00 04 67 41 4d 41 00 00 b1 8f 0b fc 61 05 00 00  ..gAMA......a...
00000040: 00 09 70 48 59 73 aa 00 16 25 00 00 16 25 01 49  ..pHYs...%...%.I
00000050: 52 24 f0 aa aa ff a5 ab 44 45 54 78 5e ec bd 3f  R$......DETx^..?
...
```

I think it could be an image since the presence of `.sRGB` color space. So we should try to recompose a proper header bytes for common image file types like png, jpeg...
I'd start to recover `mystery` as a png file, which requires this first 16 bytes: `89 50 4E 47 0D 0A 1A 0A 00 00 00 0D 49 48 44 52` and compared with the first ones in `mystery` some of them are in common!

To rewrite properly these header bytes let's open `mystery` with vim and modify it like this before its overwriting

```
    1 00000000: 89 50 4e 47 0d 0a 1a 0a 00 00 00 0d 49 48 44 52  .eN4........C"DR
    ...
:%!xxd -r
```

Now that file has been modified it should be recognized as a regular png one.

```sh
$ file mystery
mystery: PNG image data, 1642 x 1095, 8-bit/color RGB, non-interlaced
```
Exactly! So we are able to discover the image! But wait, an error apperas when I try to open.

Ok, currently we cannot open the image yet. A diagnostic tool like [pngcheck](http://www.libpng.org/pub/png/apps/pngcheck.html) may help us.

```
$ pngcheck -v mystery.png
File: mystery.png (202941 bytes)
  chunk IHDR at offset 0x0000c, length 13
    1642 x 1095 image, 24-bit RGB, non-interlaced
  chunk sRGB at offset 0x00025, length 1
    rendering intent = perceptual
  chunk gAMA at offset 0x00032, length 4: 0.45455
  chunk pHYs at offset 0x00042, length 9: 2852132389x5669 pixels/meter
  CRC error in chunk pHYs (computed 38d82c82, expected 495224f0)
ERRORS DETECTED in mystery.png
```

The report above shows some information about the image itself and in particular the error given on calculating CRC at `pHYs` chunk.
```
$ xxd -g1 -seek 0x00042 mystery.png
00000042: 70 48 59 73 aa 00 16 25 00 00 16 25 01 49 52 24  pHYs...%...%.IR$
...
```
As we can see, at offset `0x00042` the first four bytes of chunk type specify the payload with `aa 00 16 25 00 00 16 25 01 49 52 24`, which is reverred to decimals `2852132389` and `5669` reported by the error above. To recover the expected CRC I think we've to adjust `2852132389`.
As we did previously let's modify again with vim + xxd.

```
    1 00000000: 89 50 4e 47 0d 0a 1a 0a 00 00 00 0d 49 48 44 52  .PNG........IHDR
    2 00000010: 00 00 06 6a 00 00 04 47 08 02 00 00 00 7c 8b ab  ...j...G.....|..
    3 00000020: 78 00 00 00 01 73 52 47 42 00 ae ce 1c e9 00 00  x....sRGB.......
    4 00000030: 00 04 67 41 4d 41 00 00 b1 8f 0b fc 61 05 00 00  ..gAMA......a...
    5 00000040: 00 09 70 48 59 73 00 00 16 25 00 00 16 25 01 49  ..pHYs...%...%.I
    6 00000050: 52 24 f0 aa aa ff a5 ab 44 45 54 78 5e ec bd 3f  R$......DETx^..?
    ...
:%!xxd -r
```

Now another check is necessary to see if no more errors appear.

```
$ pngcheck -v mystery.png
File: mystery.png (202941 bytes)
  chunk IHDR at offset 0x0000c, length 13
    1642 x 1095 image, 24-bit RGB, non-interlaced
  chunk sRGB at offset 0x00025, length 1
    rendering intent = perceptual
  chunk gAMA at offset 0x00032, length 4: 0.45455
  chunk pHYs at offset 0x00042, length 9: 5669x5669 pixels/meter (144 dpi)
:  invalid chunk length (too large)
ERRORS DETECTED in mystery.png
```
Ok there's another problem, but not related to `pHYs` chunk. In fact the next chunk has a length of `aa aa ff a5` and a type "DET" which doesn't exists on [png specifications](https://en.wikipedia.org/wiki/Portable_Network_Graphics#File_format). I think is a corruption of `IDAT` type. So we're gonna to fix it!
Once patched `DET` with `IDAT` type we have obvioulsy to fix the length reported. According to the [png basics](http://www.libpng.org/pub/png/book/chapter08.html#:~:text=Every%20chunk%20has%20the%20same,redundancy%20check%20value%20(CRC).), the length field refers to the length of the data field alone, not the chunk type or CRC.

So an idea is to calculate the diff between the offset where `IDAT` data field chunk starts, which is at `0x0000005b`, and the offset where it ends, four bytes before the beginning of next chunk, which could be another `IDAT` or `IEND`. Let's check it

```
$ strings mystery | grep 'IDAT'
QIDATx^
IDAT6
IDAT
IDAT
```

Reopening with vim the next `IDAT` chunk starts at `0x00010004`. Subtracting 4 bytes to avoid CRC code of current first `IDAT` we have `0x00010000` upper limit offset.

```
$ python
Python 3.9.1 (default, Feb  6 2021, 06:49:13) 
[GCC 10.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> hex(0x00010000 - 0x0000005b)
'0xffa5'
```

Right, now we have to substitute the four bytes before `IDATA` type with `00 00 ff a5` and then check if finally the png is correct.

```
$ pngcheck -v mystery
File: mystery (202941 bytes)
  chunk IHDR at offset 0x0000c, length 13
    1642 x 1095 image, 24-bit RGB, non-interlaced
  chunk sRGB at offset 0x00025, length 1
    rendering intent = perceptual
  chunk gAMA at offset 0x00032, length 4: 0.45455
  chunk pHYs at offset 0x00042, length 9: 5669x5669 pixels/meter (144 dpi)
  chunk IDAT at offset 0x00057, length 65445
    zlib: deflated, 32K window, fast compression
  chunk IDAT at offset 0x10008, length 65524
  chunk IDAT at offset 0x20008, length 65524
  chunk IDAT at offset 0x30008, length 6304
  chunk IEND at offset 0x318b4, length 0
  additional data after IEND chunk
ERRORS DETECTED in myster
```

Apart the additional data I think we are ok. Now, if I try to open the image I obtain this

![c0rrupt](./c0rrupt.png)

Allright!!!