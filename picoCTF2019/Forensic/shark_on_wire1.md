**We found this [packet capture](https://jupiter.challenges.picoctf.org/static/483e50268fe7e015c49caf51a69063d0/capture.pcap). Recover the flag.**

```sh
$ wget https://jupiter.challenges.picoctf.org/static/483e50268fe7e015c49caf51a69063d0/capture.pcap
$ file capture.pcap
capture.pcap: pcap capture file, microsecond ts (little-endian) - version 2.4 (Ethernet, capture length 262144)
```
This challenge provides a snapshot of a packets traffic. A tool like [Wireshark](https://www.wireshark.org/) is very useful to this scope.
The hint of this challenge suggests to focus our attention on streams and Wireshark offers a feature to follow the streams for those packets of protocols like UDP, TPC, etc.

I'd start with UPD. So let's apply `upd` as filter and click to Analyze > Follow UDP. A viewer popups with the stream number 0...but there's nothing about the
flag. So let's proceed with the next stream and so on..HOLD ON! On 6th udp stream there's the following content:

```sh
picoCTF{StaT31355_636f6e6e}
```

All right, we've discovered the flag!