**There's something in the [building](https://jupiter.challenges.picoctf.org/static/011955b303f293d60c8116e6a4c5c84f/buildings.png). Can you retrieve the flag?**

Once downloaded our `buildings.png` let's inspect its content.

```sh
$ file buildings.png
buildings.png: PNG image data, 657 x 438, 8-bit/color RGBA, non-interlaced
```

Ok, so there's a correct matching between file extension and its nature. Probably a steganography tool like [zsteg](https://github.com/zed-0xff/zsteg) can deal with the embedded data inside this image.

```sh
$ zsteg -a buildings.png
b1,r,lsb,xy         .. text: "^5>R5YZrG"
b1,rgb,lsb,xy       .. text: "picoCTF{h1d1ng_1n_th3_b1t5}"
b1,abgr,msb,xy      .. file: PGP Secret Sub-key -
b2,b,lsb,xy         .. text: "XuH}p#8Iy="
b3,abgr,msb,xy      .. text: "t@Wp-_tH_v\r"
b4,r,lsb,xy         .. text: "fdD\"\"\"\" "
b4,r,msb,xy         .. text: "%Q#gpSv0c05"
...
```

Effectively zsteg has discoverd the hidden text `picoCTF{h1d1ng_1n_th3_b1t5}`, which is our flag :smile: