**Can you find the flag in `/problems/first-grep--part-ii_1_4496a9af2273007b52d4a1adec323b76/files` on the shell server? Remember to use `grep`.**

Well, let's go to the shell server. Once logged the we have to go in the directory mentioned above and find our file..

```sh
b3ll00z@pico-2019-shell1:~$ cd /problems/first-grep--part-ii_1_4496a9af2273007b52d4a1adec323b76/files
b3ll00z@pico-2019-shell1:/problems/first-grep--part-ii_1_4496a9af2273007b52d4a1adec323b76/files$ ls
files0  files1  files10  files2  files3  files4  files5  files6  files7  files8  files9
```

There are ten directories, each on with a bunch of files. Obviously the worst thing to do is to inspect manuelly every file in all directories listed :scream:

So to make things fast is much better using a little of bash scripting to save time..

```sh
b3ll00z@pico-2019-shell1:/problems/first-grep--part-ii_1_4496a9af2273007b52d4a1adec323b76/files$ for DIR in $(ls); do for FILE in $(ls $DIR); do cat $DIR/$FILE | grep 'picoCTF'; done; done
picoCTF{grep_r_to_find_this_af11356f}
```

All right, done!
