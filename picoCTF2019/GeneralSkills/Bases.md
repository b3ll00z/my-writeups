**What does this `bDNhcm5fdGgzX3IwcDM1` mean? I think it has something to do with bases.**

I think the question of this challenge suggests to a codification behind `bDNhcm5fdGgzX3IwcDM1`.
This could rely on base64 system. Since a common utility in all Unix systems is `base64`.

```sh
$ man base64

NAME
       base64 - base64 encode/decode data and print to standard output

SYNOPSIS
       base64 [OPTION]... [FILE]

DESCRIPTION
       Base64 encode or decode FILE, or standard input, to standard output.

       With no FILE, or when FILE is -, read standard input.

       Mandatory arguments to long options are mandatory for short options too.

       -d, --decode
              decode data

       -i, --ignore-garbage
              when decoding, ignore non-alphabet characters

       -w, --wrap=COLS
              wrap encoded lines after COLS character (default 76).  Use 0 to disable line wrapping

       --help display this help and exit

       --version
              output version information and exit

       The data are encoded as described for the base64 alphabet in RFC 4648.  When decoding, the input may contain newlines in addition to the bytes of the formal base64 alphabet.  Use
       --ignore-garbage to attempt to recover from any other non-alphabet bytes in the encoded stream.
...
```

As the synopsis explicits we have to wrap `bDNhcm5fdGgzX3IwcDM1` inside a file, Then we can use `base64` with `--decode` to see if effectively is the flag.

```sh
$ echo "bDNhcm5fdGgzX3IwcDM1" > flag2decode
$ base64 --decode flag2decode
l3arn_th3_r0p35%
```

Yeahhhh got it! :rocket:

Solution: `picoCTF{l3arn_th3_r0p35}`
