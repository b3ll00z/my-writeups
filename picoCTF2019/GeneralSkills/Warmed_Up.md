**What is 0x3D (base 16) in decimal (base 10)?**

```sh
$ python
Python 3.8.6 (default, Sep 30 2020, 04:00:38) 
[GCC 10.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> 0x3d
61
>>> 
```
Solution: `picoCTF{61}`
