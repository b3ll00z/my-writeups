**Can you find the flag in [file](https://jupiter.challenges.picoctf.org/static/515f19f3612bfd97cd3f0c0ba32bd864/file)? This would be really tedious to look through manually, something tells me there is a better way. You can also find the file in `/problems/first-grep_2_04dbf496b78e6c37c0097cdfef734d88` on the shell server.**

The name of this challenge is auto explicative: we have to use `grep` utility to find the flag.
So first thing to do is to copy the link to the file and download it with `wget`. Then I think it is just grep to something like `picoCTF{` and we are done..

```sh
$ wget https://jupiter.challenges.picoctf.org/static/515f19f3612bfd97cd3f0c0ba32bd864/file
$ cat file | grep 'picoCTF{'
picoCTF{grep_is_good_to_find_things_5af9d829}
```

And that's all folks! :smile:
