**I wrote you a [song](https://2019shell1.picoctf.com/static/24287456e17d7ee3a35147517fcb9305/lyrics.txt). Put it in the picoCTF{} flag format**

Ok, let's start by download the song and explore its content.

```sh
$ wget https://2019shell1.picoctf.com/static/24287456e17d7ee3a35147517fcb9305/lyrics.txt
$ cat lyrics.txt
Pico's a CTFFFFFFF
my mind is waitin
It's waitin

Put my mind of Pico into This
my flag is not found
put This into my flag
put my flag into Pico


shout Pico
shout Pico
shout Pico

My song's something
put Pico into This

Knock This down, down, down
put This into CTF

shout CTF
my lyric is nothing
Put This without my song into my lyric
Knock my lyric down, down, down

shout my lyric

Put my lyric into This
Put my song with This into my lyric
Knock my lyric down

shout my lyric

Build my lyric up, up ,up

shout my lyric
shout Pico
shout It

Pico CTF is fun
security is important
Fun is fun
Put security with fun into Pico CTF
Build Fun up
shout fun times Pico CTF
put fun times Pico CTF into my song

build it up

shout it
shout it

build it up, up
shout it
shout Pico
```

The lyrics up require a little of interpretation to find the flag...the hint of this challenge states "Do you think you can master rockstar?"
What this means? It's not so clear...but wait a moment! Does it means that `rockstar` is a kind of esoterical language?
 
Well, effectively the lyrics contain statemens like `put <something> into <something_else>` or `shout <something>` that reminds me instructions,,
Maybe Google can help us. Let's search for `rockstar esoteric language`...I can't believe it! The first result points to [this](https://codewithrockstar.com/). I didn't know that Rockstar is a language for writing programs as metal power ballads, this is f***ing awesome!!! :metal: :metal:

Ok let's go to `TRY IT` section of Rockstar website and paste lyrics.txt content..the output given is the following sequence `114, 114, 114, 111, 99, 107, 110, 114, 110, 48, 49, 49, 51, 114`.

All of this numbers reminds me ASCII printable characters, let's open a Python shell and convert them..
```sh
$ python
Python 3.8.6 (default, Sep 30 2020, 04:00:38) 
[GCC 10.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> "".join([chr(number) for number in [114, 114, 114, 111, 99, 107, 110, 114, 110, 48, 49, 49, 51, 114]])
'rrrocknrn0113r'
>>> 
```
Done! It was a very funny challenge :smile:

Solution: `picoCTF{rrrocknrn0113r}`

