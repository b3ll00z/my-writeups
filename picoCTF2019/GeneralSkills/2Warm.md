**Can you convert the number 42 (base 10) to binary (base 2)?**

To make things faster let's open the terminal and prompt `python` shell
```sh
$ python
Python 3.8.6 (default, Sep 30 2020, 04:00:38) 
[GCC 10.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> bin(42)
'0b101010'
>>> 
```
Solution: `picoCTF{101010}`
