**I've used a super secret mind trick to hide this file. Maybe something lies in `/problems/where-is-the-file_2_f1aa319cafd4b55ee4a60c1ba65255e2`.**

This challenge requires to use the server shell, so there we login and go to the path specified above.
```sh
b3ll00z@pico-2019-shell1:/problems/where-is-the-file_2_f1aa319cafd4b55ee4a60c1ba65255e2$ ls
b3ll00z@pico-2019-shell1:/problems/where-is-the-file_2_f1aa319cafd4b55ee4a60c1ba65255e2$
```

Apparently it seems there is nothing here. However is common in Unix systems to name a config file with a dot as first character.
This trick makes a file invisible, unless we specify a `ls` with `-a` options in order to have a full and complete list of whole files
in the current directory. So let's try!

```sh
b3ll00z@pico-2019-shell1:/problems/where-is-the-file_2_f1aa319cafd4b55ee4a60c1ba65255e2$ ls -a
.  ..  .cant_see_me
```

Very good, we found our flag. Now just see its content and we're done.

```
b3ll00z@pico-2019-shell1:/problems/where-is-the-file_2_f1aa319cafd4b55ee4a60c1ba65255e2$ cat .cant_see_me 
picoCTF{w3ll_that_d1dnt_w0RK_30444bc6}
```

Alright, done!
