**To get truly 1337, you must understand different data encodings, such as hexadecimal or binary.**
**Can you get the flag from this program to prove you are on the way to becoming 1337? Connect with `nc jupiter.challenges.picoctf.org 29956`**

Ok, let's do it!
```sh
$ nc jupiter.challenges.picoctf.org 29956
Let us see how data is stored
light
Please give the 01101100 01101001 01100111 01101000 01110100 as a word.
...
you have 45 seconds.....

Input:
```

Now before we go on it's better to open a convertitor instead to write a converter script in Python or other languages..

I found [this](https://onlineasciitools.com/) very helpful, which can translet from types like binary, hexadecimal, octals, ecc.. to a ASCII printable text.
We have to go fast because after 45 seconds the input required change value have to be quick to copy and paste...don't judge me, I just want to reach flag asap  :stuck_out_tongue_winking_eye:

Ready to retry..

```sh
$ nc jupiter.challenges.picoctf.org 29956
Let us see how data is stored
lime
Please give the 01101100 01101001 01101101 01100101 as a word.
...
you have 45 seconds.....

Input:
lime
```
I do not have the necessity to convert. Seeing the second line I suppose that `lime` is the translation for `01101100 01101001 01101101 0110010`.
So I put `lime` as input.

```sh
$ nc jupiter.challenges.picoctf.org 29956
Let us see how data is stored
lime
Please give the 01101100 01101001 01101101 01100101 as a word.
...
you have 45 seconds.....

Input:
lime
Please give me the 164 145 163 164 as a word.
Input:
```
Right! This time we have a string of numbers. A fast check to an ASCII table I opened in another terminal with `$ man ascii` leads me to consider
`164 145 163 164` as sequence of octal numbers, since decimal numbers arrive util 127. So let's open a new browser tab with octal to ascii translation and put the sequences..this gives me `test`!

```sh
$ nc jupiter.challenges.picoctf.org 29956
Let us see how data is stored
lime
Please give the 01101100 01101001 01101101 01100101 as a word.
...
you have 45 seconds.....

Input:
lime
Please give me the  164 145 163 164 as a word.
Input:
test
Please give me the 6c697a617264 as a word.
Input:
```
Good! Now the have an hexadeciamal number, so let's open another browser tab with hexadecimal to ascii from the same website mentioned above. This is a translation of `lizard`!

```sh
$ nc jupiter.challenges.picoctf.org 29956
Let us see how data is stored
lime
Please give the 01101100 01101001 01101101 01100101 as a word.
...
you have 45 seconds.....

Input:
lime
Please give me the  164 145 163 164 as a word.
Input:
test
Please give me the 6c697a617264 as a word.
Input:
lizard
You've beaten the challenge
Flag: picoCTF{learning_about_converting_values_b375bb16}
```

Awesome, we have found the flag!
