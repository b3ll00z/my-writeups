**We put together a bunch of resources to help you out on our website! If you go over there, you might even find a flag! [https://picoctf.com/resources](https://picoctf.com/resources)**

Opening the link and scrolling to the bottom of the page there is an appropriate flag for this competition..

Solution: `picoCTF{r3source_pag3_f1ag}`
