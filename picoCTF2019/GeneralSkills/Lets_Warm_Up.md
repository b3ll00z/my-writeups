**If I told you a word started with 0x70 in hexadecimal, what would it start with in ASCII?**

```sh
$ python
Python 3.8.6 (default, Sep 30 2020, 04:00:38) 
[GCC 10.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> chr(0x70)
'p'
>>> 
```
Solution: `picoCTF{p}`
