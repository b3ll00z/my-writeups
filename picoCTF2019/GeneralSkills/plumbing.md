**Sometimes you need to handle process data outside of a file. Can you find a way to keep the output from this program and search for the flag? Connect to `jupiter.challenges.picoctf.org 4427`**

Essentially we have to redirect the output of the connection to the specific port
```sh
$ nc jupiter.challenges.picoctf.org 4427 > flag.out
```

Now let's inspect the content captured..
```sh
$ cat flag.out
This is defintely not a flag
I don't think this is a flag either
Not a flag either
This is defintely not a flag
I don't think this is a flag either
Again, I really don't think this is a flag
Not a flag either
Again, I really don't think this is a flag
I don't think this is a flag either
I don't think this is a flag either
Not a flag either
Again, I really don't think this is a flag
This is defintely not a flag
Again, I really don't think this is a flag
Not a flag either
Not a flag either
This is defintely not a flag
....
```
Mmmmm, too words. Fortunately `grep` goes in help. Probably a simple filter can capture the flag..
```sh
$ cat flag.out | grep 'picoCTF{'
picoCTF{digital_plumb3r_5ea1fbd7}
```
Awesome, got it!
