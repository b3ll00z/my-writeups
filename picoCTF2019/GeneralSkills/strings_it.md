**Can you find the flag in file without running it? You can also find the [file](https://jupiter.challenges.picoctf.org/static/fae9ac5267cd6e44124e559b901df177/strings) in `/problems/strings-it_1_7a67382a38fc00751a6b9b29b0872813` on the shell server.**

Ok, once copied the link of `file`, let's download it with `wget` and look at its topic info.

```sh
$ wget https://jupiter.challenges.picoctf.org/static/fae9ac5267cd6e44124e559b901df177/strings
...
$ file strings
strings: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=53ef13f68d2f4f130b93d23efd97755ebe4f5c9f, not stripped
```

Well, it's an ELF executable file. Since it is required to not execute it, as the filename suggests I think the fastest way to find the flag is by using `strings` command.
In fact its man documentation says "print the sequences of printable characters in files". So let's have a try...

```sh
$ strings strings | grep "picoCTF{"
picoCTF{5tRIng5_1T_7f766a23}
```
Yes!!

