**I wrote you another [song](https://2019shell1.picoctf.com/static/ec882df63f9d0cfb8505879e0bc48c88/lyrics.txt). Put the flag in the picoCTF{} flag format**

```sh
$ wget https://2019shell1.picoctf.com/static/ec882df63f9d0cfb8505879e0bc48c88/lyrics.txt
$ cat lyrics.txt
Rocknroll is right              
Silence is wrong                
A guitar is a six-string        
Tommy's been down               
Music is a billboard-burning razzmatazz!
Listen to the music             
If the music is a guitar                  
Say "Keep on rocking!"                
Listen to the rhythm
If the rhythm without Music is nothing
Tommy is rockin guitar
Shout Tommy!                    
Music is amazing sensation 
Jamming is awesome presence
Scream Music!                   
Scream Jamming!                 
Tommy is playing rock           
Scream Tommy!       
They are dazzled audiences                  
Shout it!
Rock is electric heaven                     
Scream it!
Tommy is jukebox god            
Say it!                                     
Break it down
Shout "Bring on the rock!"
Else Whisper "That ain't it, Chief"                 
Break it down
```

Again, as mentioned in `mus1c.md` writeup this lyrics rely on Rockstar syntax. So we have to run it [here](https://codewithrockstar.com/online) and see the output.
Unfortunately this program doesn't output anything. Probably this is caused by an uncorrect syntax or something else.
A good idea is to open in a browser tab a webpage of Rockstar docs, this may clarify what is going on.

The program above does some I/O operations. `Listen` keyword in fact relies on read one line of input from `STDIN`. Instead, `Scream, Shout, Whisper` are all
aliases of `Say` that writes the content of a variable to `STDOUT`. 
In the middle of these operations there are some conditional expressions, which control the execution flow. Probably the fact the we didn't see anything before is caused by an unsatisfied boolean expression.

But wait! I've found line that catches our attention! 

`If the music is a guitar`

Music and guitar variables do not contain same value, so this is why the following code wasn't executed. Let's remove this line and see what happens..

```sh
Keep on rocking!
That ain't it, Chief
```

Now we have a message but not the flag...and this is expected. In fact if we look again there's another if-statement whose condition is false and cannot lead us
to the flag. That is `If the rhythm without Music is nothing` which has an `Else` statement connected to it. So let's remove these contional branches and rerun
again with this final modified code:

```sh
Rocknroll is right              
Silence is wrong                
A guitar is a six-string        
Tommy's been down               
Music is a billboard-burning razzmatazz!
Listen to the music             
Say "Keep on rocking!"                
Listen to the rhythm
Tommy is rockin guitar
Shout Tommy!                    
Music is amazing sensation 
Jamming is awesome presence
Scream Music!                   
Scream Jamming!                 
Tommy is playing rock           
Scream Tommy!       
They are dazzled audiences                  
Shout it!
Rock is electric heaven                     
Scream it!
Tommy is jukebox god            
Say it!                                     
Break it down
Shout "Bring on the rock!"
Break it down
```

The output give is the following:

```sh
Keep on rocking!
66
79
78
74
79
86
73
```

Well done! We have a sequence of numbers to convert to ASCII characters. It's time to open a Python shell and make the flag!

```sh
$ python
Python 3.8.6 (default, Sep 30 2020, 04:00:38) 
[GCC 10.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> "".join([chr(number) for number in [66, 79, 78, 74, 79, 86, 73]])
'BONJOVI'
>>> 
```

That's all folks!!

Solution: `picoCTF{BONJOVI}`

