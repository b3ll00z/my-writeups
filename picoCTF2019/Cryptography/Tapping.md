**Theres tapping coming in from the wires. What's it saying `nc jupiter.challenges.picoctf.org 48247`.**

```
$ nc jupiter.challenges.picoctf.org 48247 
.--. .. -.-. --- -.-. - ..-. { -- ----- .-. ... ...-- -.-. ----- -.. ...-- .---- ... ..-. ..- -. .---- ..--- -.... .---- ....- ...-- ---.. .---- ---.. .---- }
```

Looking at the given output it's recognizable the `picoCTF{...}` format of the flag, transformed by [Morse](https://en.wikipedia.org/wiki/Morse_code) encoding.

To discover the flag, an easier way is to use this [translator](https://morsedecoder.com/). Than just copy the content above and paste it inside the "Morse Code" box. The output given is `PICOCTF#M0RS3C0D31SFUN1261438181#`!

Before submitting the flag just turn "#" occurrences with curly brackets and we're done :sunglasses:

Solution: `PICOCTF{M0RS3C0D31SFUN1261438181}`
