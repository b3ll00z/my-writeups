**Decrypt this [message](https://jupiter.challenges.picoctf.org/static/7d707a443e95054dc4cf30b1d9522ef0/ciphertext).**

This is the content of the encrypted file

```sh
$ cat ciphertext
picoCTF{gvswwmrkxlivyfmgsrhnrisegl}% 
```

Since this challenge is based on [Caesar's cipher](https://en.wikipedia.org/wiki/Caesar_cipher) we have to revert back each transposed character by three positions.
However, to cover all possibile 26 let's brute force it..

```sh
$ python
>>> encrypted = "gvswwmrkxlivyfmgsrhnrisegl"
>>> for shift in range(1, 26):
...   print("Shift: {} --> picoCTF{{{}}}".format(str(shift), "".join([chr(((ord(x) - 96 - shift) % 26) + 96) for x in encrypted])))
... 
Shift: 1 --> picoCTF{furvvlqjwkhuxelfrqgmqhrdfk}
Shift: 2 --> picoCTF{etquukpivjgtwdkeqpflpgqcej}
Shift: 3 --> picoCTF{dspttjohuifsvcjdpoekofpbdi}
Shift: 4 --> picoCTF{crossingtherubicondjneoach}
Shift: 5 --> picoCTF{bqnrrhmfsgdqtahbnmcimdn`bg}
Shift: 6 --> picoCTF{apmqqglerfcps`gamlbhlcmyaf}
Shift: 7 --> picoCTF{`olppfkdqeboryf`lkagkblx`e}
Shift: 8 --> picoCTF{ynkooejcpdanqxeykj`fjakwyd}
Shift: 9 --> picoCTF{xmjnndiboc`mpwdxjiyei`jvxc}
Shift: 10 --> picoCTF{wlimmchanbylovcwihxdhyiuwb}
Shift: 11 --> picoCTF{vkhllbg`maxknubvhgwcgxhtva}
Shift: 12 --> picoCTF{ujgkkafyl`wjmtaugfvbfwgsu`}
Shift: 13 --> picoCTF{tifjj`exkyvils`tfeuaevfrty}
Shift: 14 --> picoCTF{sheiiydwjxuhkrysedt`dueqsx}
Shift: 15 --> picoCTF{rgdhhxcviwtgjqxrdcsyctdprw}
Shift: 16 --> picoCTF{qfcggwbuhvsfipwqcbrxbscoqv}
Shift: 17 --> picoCTF{pebffvatgurehovpbaqwarbnpu}
Shift: 18 --> picoCTF{odaeeu`sftqdgnuoa`pv`qamot}
Shift: 19 --> picoCTF{nc`ddtyrespcfmtn`youyp`lns}
Shift: 20 --> picoCTF{mbyccsxqdrobelsmyxntxoykmr}
Shift: 21 --> picoCTF{laxbbrwpcqnadkrlxwmswnxjlq}
Shift: 22 --> picoCTF{k`waaqvobpm`cjqkwvlrvmwikp}
Shift: 23 --> picoCTF{jyv``punaolybipjvukqulvhjo}
Shift: 24 --> picoCTF{ixuyyotm`nkxahoiutjptkugin}
Shift: 25 --> picoCTF{hwtxxnslymjw`gnhtsiosjtfhm}
```

If you look above the only meaningful flag is at `Shift: 4 --> picoCTF{crossingtherubicondjneoach}` :sunglasses:
