**What do the [flags](https://jupiter.challenges.picoctf.org/static/fbeb5f9040d62b18878d199cdda2d253/flag.png) mean?**

This is the image content obtained once opened `flag.png`.

![flag](./flag.png)

The flag is composed by a sequence of flags compliant with [International maritime signal flags](https://en.wikipedia.org/wiki/International_maritime_signal_flags).
At this link we can easily recognize to each one the letter correspondent. In fact, we can obtain `PICOCTF{F1AG5AND5TUFF}`.
