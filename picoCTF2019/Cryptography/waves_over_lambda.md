**We made a lot of substitutions to encrypt this. Can you decrypt it? Connect with `nc jupiter.challenges.picoctf.org 39894`.**

```
$ nc jupiter.challenges.picoctf.org 39894
-------------------------------------------------------------------------------
njazspmt rwsw ft ejvs xhpz - xswuvwane_ft_n_jyws_hpiokp_pzxhnzmevw
-------------------------------------------------------------------------------
phwbwe xejkjsjyfmnr lpspipdjy cpt mrw mrfsk tja jx xejkjs gpyhjyfmnr lpspipdjy, p hpak jcaws cwhh lajca fa jvs kftmsfnm fa rft jca kpe, pak tmfhh swiwiowswk pijaz vt jcfaz mj rft zhjjie pak mspzfn kwpmr, crfnr rpggwawk mrfsmwwa ewpst pzj, pak crfnr f trphh kwtnsfow fa fmt gsjgws ghpnw. xjs mrw gswtwam f cfhh jahe tpe mrpm mrft hpakjcawsxjs tj cw vtwk mj nphh rfi, phmrjvzr rw rpskhe tgwam p kpe jx rft hfxw ja rft jca wtmpmwcpt p tmspazw megw, ewm jaw gswmme xswuvwamhe mj ow iwm cfmr, p megw poqwnm pak yfnfjvt pak pm mrw tpiw mfiw twatwhwtt. ovm rw cpt jaw jx mrjtw twatwhwtt gwstjat crj psw ywse cwhh npgpohw jx hjjlfaz pxmws mrwfs cjshkhe pxxpfst, pak, pggpswamhe, pxmws ajmrfaz whtw. xejkjs gpyhjyfmnr, xjs fatmpanw, owzpa cfmr awbm mj ajmrfaz; rft wtmpmw cpt jx mrw tiphhwtm; rw spa mj kfaw pm jmrws iwa't mpohwt, pak xptmwawk ja mrwi pt p mjpke, ewm pm rft kwpmr fm pggwpswk mrpm rw rpk p rvakswk mrjvtpak sjvohwt fa rpsk nptr. pm mrw tpiw mfiw, rw cpt phh rft hfxw jaw jx mrw ijtm twatwhwtt, xpamptmfnph xwhhjct fa mrw crjhw kftmsfnm. f swgwpm, fm cpt ajm tmvgfkfmemrw ipqjsfme jx mrwtw xpamptmfnph xwhhjct psw trswck pak famwhhfzwam wajvzrovm qvtm twatwhwttawtt, pak p gwnvhfps apmfjaph xjsi jx fm.
```

The fact that our ciphertext is made by a lot of substitutions cannot be specific about what kind of substitution is made. Despite the key is unknown we
can recover it with this [solver](https://planetcalc.com/8047/). Once pasted the ciphertext above the output given is:

```
-------------------------------------------------------------------------------
CONGRATS HERE IS YOUR FLAG - FREQUENCY_IS_C_OVER_LAMBDA_AGFLCGTYUE
-------------------------------------------------------------------------------
ALEXEY FYODOROVITCH KARAMAZOV WAS THE THIRD SON OF FYODOR PAVLOVITCH KARAMAZOV, A LAND OWNER WELL KNOWN IN OUR DISTRICT IN HIS OWN DAY, AND STILL REMEMBERED AMONG US OWING TO HIS GLOOMY AND TRAGIC DEATH, WHICH HAPPENED THIRTEEN YEARS AGO, AND WHICH I SHALL DESCRIBE IN ITS PROPER PLACE. FOR THE PRESENT I WILL ONLY SAY THAT THIS LANDOWNERFOR SO WE USED TO CALL HIM, ALTHOUGH HE HARDLY SPENT A DAY OF HIS LIFE ON HIS OWN ESTATEWAS A STRANGE TYPE, YET ONE PRETTY FREQUENTLY TO BE MET WITH, A TYPE ABJECT AND VICIOUS AND AT THE SAME TIME SENSELESS. BUT HE WAS ONE OF THOSE SENSELESS PERSONS WHO ARE VERY WELL CAPABLE OF LOOKING AFTER THEIR WORLDLY AFFAIRS, AND, APPARENTLY, AFTER NOTHING ELSE. FYODOR PAVLOVITCH, FOR INSTANCE, BEGAN WITH NEXT TO NOTHING; HIS ESTATE WAS OF THE SMALLEST; HE RAN TO DINE AT OTHER MEN'S TABLES, AND FASTENED ON THEM AS A TOADY, YET AT HIS DEATH IT APPEARED THAT HE HAD A HUNDRED THOUSAND ROUBLES IN HARD CASH. AT THE SAME TIME, HE WAS ALL HIS LIFE ONE OF THE MOST SENSELESS, FANTASTICAL FELLOWS IN THE WHOLE DISTRICT. I REPEAT, IT WAS NOT STUPIDITYTHE MAJORITY OF THESE FANTASTICAL FELLOWS ARE SHREWD AND INTELLIGENT ENOUGHBUT JUST SENSELESSNESS, AND A PECULIAR NATIONAL FORM OF IT.
```

Since our flag is on lowercase the flag given should lowercase too. In fact submitted as is won't be accepted.

Solution: `frequency_is_c_over_lambda_agflcgtyue`
