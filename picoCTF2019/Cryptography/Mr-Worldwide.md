**A musician left us a [message](https://jupiter.challenges.picoctf.org/static/d5570d48262dbba2a31f2a940409ad9d/message.txt). What's it mean?**

```
$ cat message.txt
picoCTF{(35.028309, 135.753082)(46.469391, 30.740883)(39.758949, -84.191605)(41.015137, 28.979530)(24.466667, 54.366669)(3.140853, 101.693207)_(9.005401, 38.763611)(-3.989038, -79.203560)(52.377956, 4.897070)(41.085651, -73.858467)(57.790001, -152.407227)(31.205753, 29.924526)}%
```

It seems that all flag letters are being substituted with `(latitude, longitude)` pairs. To discover each of them let's open our friend [Google Maps](https://www.google.it/maps) and note each location pointed.

```
(35.028309, 135.753082) -> Nakanocho, Kamigyo Ward, Kyoto, Prefettura di Kyoto 602-0958, Giappone
(46.469391 30.740883) -> Odessa, Ucraina, 65000
(39.758949 -84.191605) -> Dayton, OH 45402, Stati Uniti
(41.015137, 28.979530) -> Hoca Paşa, 34110 Fatih/Provincia di Istanbul, Turchia
(24.466667, 54.366669) -> Hazza' Bin Zayed the First St - Al Manhal - Abu Dhabi - Emirati Arabi Uniti
(3.140853, 101.693207) -> 50480 Kuala Lumpur, Territorio Federale di Kuala Lumpur, Malesia
(9.005401, 38.763611) -> Kirkos, Addis Abeba, Etiopia
(-3.989038, -79.203560) -> Av Nueva Loja, Loja, Ecuador
(52.377956, 4.897070) -> Martelaarsgracht 5, 1012 TN Amsterdam, Paesi Bassi
(41.085651, -73.858467) -> 188 vida activa, Sleepy Hollow, NY 10591, Stati Uniti
(57.790001, -152.407227) -> Kodiak, AK 99615, Stati Uniti
(31.205753, 29.924526) -> Faculty Of Engineering, Al Azaritah WA Ash Shatebi, Qism Bab Sharqi, Alexandria Governorate, Egitto
```

I think that taking each initial letter of all the towns mentioned above should compose the flag.
In fact, we obtain `KODIAKALASKA`, which combined with the flag message is `picoCTF{KODIAK_ALASKA}` :fire:
