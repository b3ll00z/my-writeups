**The [numbers](https://jupiter.challenges.picoctf.org/static/f209a32253affb6f547a585649ba4fda/the_numbers.png)... what do they mean?**

This challenge relates to `the_numbers.png` image file. Once opened I got this picture:

![the_numbers](./the_numbers.png)

We have a sequence of numbers, each of them representing a specific character of our flag. I'm convinced that we're talking about a simple mapping between
alphabet letters and the respective numerical positions.
In fact the first 7 letters encodes "PICOCTF". The fact that they are in uppercase is suggested by the hint.
A simple program can find our flag easily.

```sh
$ python
Python 3.9.1 (default, Feb  6 2021, 06:49:13) 
[GCC 10.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> print("PICOCTF{" + "".join(chr(x + 64) for x in [20, 8, 5, 14, 21, 13, 2, 5, 18, 19, 13, 1, 19, 15, 14]) + "}")
PICOCTF{THENUMBERSMASON}
```

oooh yes, flag captured!
