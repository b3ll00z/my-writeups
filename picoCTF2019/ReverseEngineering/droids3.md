**Find the pass, get the flag. Check out this [file](https://jupiter.challenges.picoctf.org/static/06318765139795831859f843dd56ce60/three.apk).**

This is another rev challange related to an apk file. So I go to the point immediately and I suggest you to look at setup operations explained in the previous `droids*.md` writeups.

Once downloaded the reversed java code of `three.apk` we have the following `MainActivity.java` source:

```java
package com.hellocmu.picoctf;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    Button button;
    Context ctx;
    TextView text_bottom;
    EditText text_input;
    TextView text_top;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main);
        this.text_top = (TextView) findViewById(R.id.text_top);
        this.text_bottom = (TextView) findViewById(R.id.text_bottom);
        this.text_input = (EditText) findViewById(R.id.text_input);
        this.ctx = getApplicationContext();
        System.loadLibrary("hellojni");
        this.text_top.setText(R.string.hint);
    }

    public void buttonClick(View view) {
        this.text_bottom.setText(FlagstaffHill.getFlag(this.text_input.getText().toString(), this.ctx));
    }
}
```

Again there's the common droids scenario, where the main activity shows a text field and a button with a click-listener related.
To discover its functionality let's check `FlagstaffHill.java`.

```java
package com.hellocmu.picoctf;

import android.content.Context;

public class FlagstaffHill {
    public static native String cilantro(String str);

    public static String nope(String input) {
        return "don't wanna";
    }

    public static String yep(String input) {
        return cilantro(input);
    }

    public static String getFlag(String input, Context ctx) {
        return nope(input);
    }
}
```

Well, `getFlag()` wraps the invocation of `nope(input)` and whatever input string takes it always returns "don't wanna".
This is not the real flag, because if we try to submit something "don't wanna" or "picoCTF{don't wanna}" it would be rejected.

To address the correct flag I'm sure we just have to make `getFlag()` wrapping `yep(input)` invocation.
A tool like [apktool](https://ibotpeaches.github.io/Apktool/) may help us. Once followed the installation process let's decode `three.apk`

```sh
$ apktool d three.apk
I: Using Apktool 2.5.0 on three.apk
I: Loading resource table...
I: Decoding AndroidManifest.xml with resources...
I: Loading resource table from file...
I: Regular manifest package...
I: Decoding file-resources...
I: Decoding values */* XMLs...
I: Baksmaling classes.dex...
I: Copying assets and libs...
I: Copying unknown files...
I: Copying original files..
$ ls
three  three.apk
```

Right, so now we have a `three/` directory which contains all source file decompiled in SMALI assembly language, referred to the decompiled `.dex` (Dalvik Executable) files included in our `three.apk` file. So we've just to pick the one referred to `FlagstaffHill` class and modify `getFlag()` method like below:

```smali
.class public Lcom/hellocmu/picoctf/FlagstaffHill;
.super Ljava/lang/Object;
.source "FlagstaffHill.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native cilantro(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public static getFlag(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;
    .param p1, "ctx"    # Landroid/content/Context;

    .line 19
    invoke-static {p0}, Lcom/hellocmu/picoctf/FlagstaffHill;->yep(Ljava/lang/String;)Ljava/lang/String; # here we apply yep(input) invocation

    move-result-object v0

    .line 20
    .local v0, "flag":Ljava/lang/String;
    return-object v0
.end method

.method public static nope(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .line 11
    const-string v0, "don\'t wanna"

    return-object v0
.end method

.method public static yep(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "input"    # Ljava/lang/String;

    .line 15
    invoke-static {p0}, Lcom/hellocmu/picoctf/FlagstaffHill;->cilantro(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
```

Now we have to rebuild our patched apk. Before proceed we have to take care of a [disclaimer](https://ibotpeaches.github.io/Apktool/documentation/#building) in Apktool documentation, which forces us to resign the application before launching it in a device or emulator.
The following commands are used to create a key to add in local keystore, build the patched apk, sign it with our key and install the app in our device.

```sh
$ keytool -genkey -v -keystore my-release-key.keystore -alias alias_name -keyalg RSA -keysize 2048 -validity 10000
$ apktool b -f -d three -o three-patch.apk
$ jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore my-release-key.keystore three-patch.apk alias_name
$ adb install three-patch.apk
```

And this is with I obtain after put a useless string once I open the app

![droids3](./droids3.jpeg)

Wow, flag captured :fire:
