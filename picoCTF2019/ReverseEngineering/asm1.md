**What does `asm1(0x8be)` return? Submit the flag as a hexadecimal value (starting with '0x'). NOTE: Your submission for this question will NOT be in the normal flag format. [Source](https://jupiter.challenges.picoctf.org/static/66c927e32f3d7be7a62d13a7c2250943/test.S)**

```
$ wget https://jupiter.challenges.picoctf.org/static/66c927e32f3d7be7a62d13a7c2250943/test.S
$ cat test.S
asm1:
	<+0>:	push   ebp
	<+1>:	mov    ebp,esp
	<+3>:	cmp    DWORD PTR [ebp+0x8],0x71c
	<+10>:	jg     0x512 <asm1+37>
	<+12>:	cmp    DWORD PTR [ebp+0x8],0x6cf
	<+19>:	jne    0x50a <asm1+29>
	<+21>:	mov    eax,DWORD PTR [ebp+0x8]
	<+24>:	add    eax,0x3
	<+27>:	jmp    0x529 <asm1+60>
	<+29>:	mov    eax,DWORD PTR [ebp+0x8]
	<+32>:	sub    eax,0x3
	<+35>:	jmp    0x529 <asm1+60>
	<+37>:	cmp    DWORD PTR [ebp+0x8],0x8be
	<+44>:	jne    0x523 <asm1+54>
	<+46>:	mov    eax,DWORD PTR [ebp+0x8]
	<+49>:	sub    eax,0x3
	<+52>:	jmp    0x529 <asm1+60>
	<+54>:	mov    eax,DWORD PTR [ebp+0x8]
	<+57>:	add    eax,0x3
	<+60>:	pop    ebp
	<+61>:	ret
```
This challenge advices to follow the conditionals flow of inside `asm1` called with `0x8be`.

After the prologue instructions we have the first comparison at `<+3>` line, where `0x8be` is referred by `DWORD PTR [ebp+0x8]`.
Since `0x8be` is greater than `0x71c` the program jumps to `<asm1+37>` instruction.

Now the current comparison is `cmp DWORD PTR [ebp+0x8],0x8be` which compares 0x8be with itself. So being both operands equal the program executes

```nasm
mov    eax,DWORD PTR [ebp+0x8]
sub    eax,0x3
```

As reported, the register `eax` is initialized by `DWORD PTR [ebp+0x8]`, that is `0x8be`. Then `eax` is decremented by 0x3, so the final value is `0x8bb`.
The program than jumps to epilogue instructions.

Solution: `0x8bb`

