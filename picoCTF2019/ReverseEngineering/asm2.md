**What does `asm2(0xb,0x2e)` return? Submit the flag as a hexadecimal value (starting with '0x'). NOTE: Your submission for this question will NOT be in the normal flag format. [Source](https://jupiter.challenges.picoctf.org/static/717467c8c8b4332ea5873ad8fe7b2dad/test.S)**

```
$ wget https://jupiter.challenges.picoctf.org/static/717467c8c8b4332ea5873ad8fe7b2dad/test.S
$ cat test.S
asm2:
	<+0>:	push   ebp
	<+1>:	mov    ebp,esp
	<+3>:	sub    esp,0x10
	<+6>:	mov    eax,DWORD PTR [ebp+0xc]
	<+9>:	mov    DWORD PTR [ebp-0x4],eax
	<+12>:	mov    eax,DWORD PTR [ebp+0x8]
	<+15>:	mov    DWORD PTR [ebp-0x8],eax
	<+18>:	jmp    0x509 <asm2+28>
	<+20>:	add    DWORD PTR [ebp-0x4],0x1
	<+24>:	sub    DWORD PTR [ebp-0x8],0xffffff80
	<+28>:	cmp    DWORD PTR [ebp-0x8],0x63f3
	<+35>:	jle    0x501 <asm2+20>
	<+37>:	mov    eax,DWORD PTR [ebp-0x4]
	<+40>:	leave  
	<+41>:	ret    
```
This challenge advices to follow the conditionals flow of inside `asm2` called with `0xb` and `0x2e`.

After the prologue instructions and a stack space reservation of 16 bytes we have the following assignements

```nasm
mov eax,DWORD PTR [ebp+0xc]
mov DWORD PTR [ebp-0x4],eax
```

where `eax` is filled with `0x2e`, which is the second argument obtained by `DWORD PTR [ebp+0xc]`. Then `eax` is used to initialized
a local variable reserved in `DWORD PTR [ebp-0x4]`.
The next two instructions assign to `eax` the first `asm2` argument `0xb` that is used to initialize another local variable pointed in `DWORD PTR [ebp-0x8]`.

The program then jumps to `cmp    DWORD PTR [ebp-0x8],0x63f3`, which compares `0xb` with `0x63f3`. Since the former is less than latter begins a loop to
these instructions

```nasm
add    DWORD PTR [ebp-0x4],0x1
sub    DWORD PTR [ebp-0x8],0xffffff80
cmp    DWORD PTR [ebp-0x8],0x63f3
jle    0x501 <asm2+20>
```

the loop terminates until `DWORD PTR [ebp-0x8]` variable exceedes `0x63f3`. For this reason I've prepared the following program to do work for us :smile:

```c
#include <stdio.h>

int main() {
  int flag = 0x2e;
  int actual = 0xb;
  int target = 0x63f3;

  while (actual <= target) {
    flag += 1;
    actual -= 0xffffff80;
  }

  printf("Number of rounds: %d\n", flag - 0x2e);
  printf("Flag: 0x%x\n", flag); 
  return 0;
}
```

and let's capture the flag

```sh
$ gcc -o writeup writeup.c
$ ./writeup
Number of rounds: 200
Flag: 0xf6
```

That's all folks!!

