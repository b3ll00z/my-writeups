**What does `asm3(0xba6c5a02,0xd101e3dd,0xbb86a173)` return? Submit the flag as a hexadecimal value (starting with '0x'). NOTE: Your submission for this question will NOT be in the normal flag format. [Source](https://jupiter.challenges.picoctf.org/static/cb753ae52bca4aa303deca5fbfb01bfb/test.S)**

```nasm
asm3:
	<+0>:	push   ebp
	<+1>:	mov    ebp,esp
	<+3>:	xor    eax,eax
	<+5>:	mov    ah,BYTE PTR [ebp+0xb]
	<+8>:	shl    ax,0x10
	<+12>:	sub    al,BYTE PTR [ebp+0xd]
	<+15>:	add    ah,BYTE PTR [ebp+0xc]
	<+18>:	xor    ax,WORD PTR [ebp+0x12]
	<+22>:	nop
	<+23>:	pop    ebp
	<+24>:	ret
```

To be honest I think the fastest way is to literally create an equivalent assembly file and execute it with the parameters requested.
So let's organize the assembly code below..

```nasm
.intel_syntax noprefix
.global asm3

asm3:
	push   ebp
	mov    ebp,esp
	xor    eax,eax
	mov    ah,BYTE PTR [ebp+0xb]
	shl    ax,0x10
	sub    al,BYTE PTR [ebp+0xd]
	add    ah,BYTE PTR [ebp+0xc]
	xor    ax,WORD PTR [ebp+0x12]
	nop
	pop    ebp
	ret
```

in order to be used by this C program consumer

```c
#include <stdio.h>

int asm3 (int, int, int);

int main() {
  printf("Result: 0x%x\n", asm3(0xba6c5a02,0xd101e3dd,0xbb86a173));
  return 0;
}
```

Now we just only have to compile and run it..

```sh
$ gcc -masm=intel -m32 -c asm3.S -o asm3.o 
$ gcc -m32 -c writeup.c -o writeup.o
$ gcc -m32 writeup.o asm3.o -o writeup
$ ./writeup
Result: 0x669b
```

Good, flag captured!


