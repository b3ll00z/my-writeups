**This vault uses an XOR encryption scheme. The source code for this vault is here: [VaultDoor6.java](https://jupiter.challenges.picoctf.org/static/86e94cc555b2ca7375424c884ef581a6/VaultDoor6.java)**

```java
import java.util.*;

class VaultDoor6 {
    public static void main(String args[]) {
        VaultDoor6 vaultDoor = new VaultDoor6();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter vault password: ");
        String userInput = scanner.next();
	    String input = userInput.substring("picoCTF{".length(),userInput.length()-1);
	    if (vaultDoor.checkPassword(input)) {
	       System.out.println("Access granted.");
	    } else {
	       System.out.println("Access denied!");
        }
    }

    // Dr. Evil gave me a book called Applied Cryptography by Bruce Schneier,
    // and I learned this really cool encryption system. This will be the
    // strongest vault door in Dr. Evil's entire evil volcano compound for sure!
    // Well, I didn't exactly read the *whole* book, but I'm sure there's
    // nothing important in the last 750 pages.
    //
    // -Minion #3091
    public boolean checkPassword(String password) {
        if (password.length() != 32) {
            return false;
        }
        byte[] passBytes = password.getBytes();
        byte[] myBytes = {
            0x3b, 0x65, 0x21, 0xa , 0x38, 0x0 , 0x36, 0x1d,
            0xa , 0x3d, 0x61, 0x27, 0x11, 0x66, 0x27, 0xa ,
            0x21, 0x1d, 0x61, 0x3b, 0xa , 0x2d, 0x65, 0x27,
            0xa , 0x66, 0x36, 0x30, 0x67, 0x6c, 0x64, 0x6c,
        };
        for (int i=0; i<32; i++) {
            if (((passBytes[i] ^ 0x55) - myBytes[i]) != 0) {
                return false;
            }
        }
        return true;
    }
}
```

Obviously the heart of this challenge is inside `checkPassword()` method, where the password given by the user is collected in `passBytes` array and checked by a [XOR Encryption Scheme](https://www.101computing.net/xor-encryption-algorithm/). As the article suggests there three main reasons to make strong this encryption scheme:

- It is based on a long key that will not repeat itself. (e.g. a key that contains as many bits/characters as the plaintext)
- A new key is randomly generated for any new communication.
- The key is kept secret by both the sender and the receiver.

However we can acquire by `checkPassword()` itself the knowledge about the key (`0x55`) and the expected encrypted password inside `myBytes` array. To exploit the flag we can still use the xor operation in order to revert back the expected plaintext. So let's prepare a proper script..

```python
if __name__ == "__main__":
  myBytes = [
    0x3b, 0x65, 0x21, 0xa , 0x38, 0x0 , 0x36, 0x1d,
    0xa , 0x3d, 0x61, 0x27, 0x11, 0x66, 0x27, 0xa ,
    0x21, 0x1d, 0x61, 0x3b, 0xa , 0x2d, 0x65, 0x27,
    0xa , 0x66, 0x36, 0x30, 0x67, 0x6c, 0x64, 0x6c
  ]
  
  flag = [chr(0x55 ^ byte) for byte in myBytes]
  print("picoCTF{" + "".join(flag) + "}")
```

..and crack the flag!

```sh
$ python writeup.py
picoCTF{n0t_mUcH_h4rD3r_tH4n_x0r_3ce2919}
```
