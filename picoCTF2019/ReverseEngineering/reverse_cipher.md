**We have recovered a [binary](https://jupiter.challenges.picoctf.org/static/48babf8f8c4c6b8baf336680ea5b9ddf/rev) and a [text](https://jupiter.challenges.picoctf.org/static/48babf8f8c4c6b8baf336680ea5b9ddf/rev_this) file. Can you reverse the flag.**

This challenge recommends the usage of [Ghidra](https://ghidra-sre.org/) reverse tool. So once downloaded let's create a project
and import `rev` binary. This is the C-like pseoudocode of its `main()` function.

```c
void main(void) {
  size_t sVar1;
  char local_58 [23];
  char local_41;
  int local_2c;
  FILE *local_28;
  FILE *local_20;
  uint local_14;
  int local_10;
  char local_9;
  
  local_20 = fopen("flag.txt","r");
  local_28 = fopen("rev_this","a");
  if (local_20 == (FILE *)0x0) {
    puts("No flag found, please make sure this is run on the server");
  }
  if (local_28 == (FILE *)0x0) {
    puts("please run this on the server");
  }
  sVar1 = fread(local_58,0x18,1,local_20);
  local_2c = (int)sVar1;
  if ((int)sVar1 < 1) {
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
  local_10 = 0;
  while (local_10 < 8) {
    local_9 = local_58[local_10];
    fputc((int)local_9,local_28);
    local_10 = local_10 + 1;
  }
  local_14 = 8;
  while ((int)local_14 < 0x17) {
    if ((local_14 & 1) == 0) {
      local_9 = local_58[(int)local_14] + '\x05';
    }
    else {
      local_9 = local_58[(int)local_14] + -2;
    }
    fputc((int)local_9,local_28);
    local_14 = local_14 + 1;
  }
  local_9 = local_41;
  fputc((int)local_41,local_28);
  fclose(local_28);
  fclose(local_20);
  return;
}
```

At first sight we need to create a `flag.txt` file. In fact, the `local_20` is used to contain it's file pointer. Subsequently, `local_20` is
used in this code block

```c
  sVar1 = fread(local_58,0x18,1,local_20);
  local_2c = (int)sVar1;
  if ((int)sVar1 < 1) {
                    /* WARNING: Subroutine does not return */
    exit(0);
  }
```

this means that `local_20` is a stream buffer used to fill `local_58` with 1 element of 0x18 (24) bytes. The `fread()` returns in `sVar1` the total number of elements successfully read. If `sVar1` has no elements the program exits.
So the idea is to create a `flag.txt` with 24 characters of `rev_this`. Let's do it!

```sh
$ cat rev_this >> flag.txt
```

Before executing the `rev` binary let's continue with its analysis. 

```c
  local_10 = 0;
  while (local_10 < 8) {
    local_9 = local_58[local_10];
    fputc((int)local_9,local_28);
    local_10 = local_10 + 1;
  }
``` 

The above code block takes the first 8 characters of `flag.txt` and writes them to `local_9` file pointer of `rev_this`.
Then the program continues with the following code block
```c
  local_14 = 8;
  while ((int)local_14 < 0x17) {
    if ((local_14 & 1) == 0) {
      local_9 = local_58[(int)local_14] + '\x05';
    }
    else {
      local_9 = local_58[(int)local_14] + -2;
    }
    fputc((int)local_9,local_28);
    local_14 = local_14 + 1;
  }
  local_9 = local_41;
  fputc((int)local_41,local_28);
```

In this case the code above takes all `flag.txt` bytes from 8th to 23th position.
For each position, if it is even the respective character in `flag.txt` is added with '\x05', otherwise is subtracted by 2. Finally each modified character is written to `rev_this`.
Essentially the idea is to effectively invert this "cipher" operations. For this reason it is reasonable to
write a program which reverse, to even position from adding to subtracting '\x05' and from subtracting to adding 2.

```c
#include <stdio.h>
#include <stdlib.h>

int main() {
  char *local_58 = "picoCTF{w1{1wq8/7376j.:}";
  char *local_28 = (char *) malloc(sizeof local_58);
  char local_9;

  int local_10 = 0;
  while (local_10 < 8) {
    local_9 = local_58[local_10];
    local_28[local_10] = (int)local_9;
    local_10 = local_10 + 1;
  }
  int local_14 = 8;
  while ((int)local_14 < 0x17) {
    if ((local_14 & 1) == 0) {
      local_9 = local_58[(int)local_14] - '\x05';
    }
    else {
      local_9 = local_58[(int)local_14] + 2;
    }
    local_28[local_14] = (int)local_9;
    local_14 = local_14 + 1;
  }

  printf("%s}\n", local_28);
  return 0;
}
```
Let's see what happens

```sh
$ gcc -o writeup writeup.c
$ ./writeup
picoCTF{r3v3rs312528e05}
```

And that's all folks!! :smile:
