**Where do droid logs go. Check out this [file](https://jupiter.challenges.picoctf.org/static/02bcd73e630f50ef0b12bcdad9d84e0d/zero.apk).**

All right, let's start by downloading and inspecting this file.

```sh
$ wget https://jupiter.challenges.picoctf.org/static/02bcd73e630f50ef0b12bcdad9d84e0d/zero.apk
$ file zero.apk
zero.apk: Zip archive data, at least v1.0 to extract
```
An `.apk` file is commonly referred to a binary of an Android application. There are different ways to get the source code from an apk file.
For this challenge I would consider this online [decompiler](https://www.decompiler.com/). So let't drop it the `zero.apk` and download the zip
file returned.

Now it's time to look at the source code.
```sh
$ unzip -q zero.apk.zip
$ ls
resources  sources  zero.apk.zip
```

Now to retrieve the flag I think we've to look at `MainActivity.java` file which is related to the first content the user views once the app is opened.

```sh
$ find sources -type f -name 'MainActivity.java'
sources/com/hellocmu/picoctf/MainActivity.java
```

Good, let's inspect the main activity source code.

```java
package com.hellocmu.picoctf;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    Button button;
    Context ctx;
    TextView text_bottom;
    EditText text_input;
    TextView text_top;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main);
        this.text_top = (TextView) findViewById(R.id.text_top);
        this.text_bottom = (TextView) findViewById(R.id.text_bottom);
        this.text_input = (EditText) findViewById(R.id.text_input);
        this.ctx = getApplicationContext();
        System.loadLibrary("hellojni");
        this.text_top.setText(R.string.hint);
    }

    public void buttonClick(View view) {
        this.text_bottom.setText(FlagstaffHill.getFlag(this.text_input.getText().toString(), this.ctx));
    }
}
```

As you can see there is `buttonClick()` listener method, which is invoked every time the user click on the button shown on screen.
Inside this method the crucial point is the check given by `FlagstaffHill.getFlag(this.text_input.getText().toString(), this.ctx)`.
So let's find this `FlagstaffHill` class..

```sh
$ find sources -type f -name 'FlagstaffHill.java'
sources/com/hellocmu/picoctf/FlagstaffHill.java
```

as expected this class is inside the same package containing `MainActivity.java`, and that is its source code

```java
package com.hellocmu.picoctf;

import android.content.Context;
import android.util.Log;

public class FlagstaffHill {
    public static native String paprika(String str);

    public static String getFlag(String input, Context ctx) {
        Log.i("PICO", paprika(input));
        return "Not Today...";
    }
}
```

Oook, we've discovered that every input the user submits we always get "Not Today" returned to the main activity. But there's always a log of `paprika(input)` invocation.
As we see by its signature there's a native keyword, indicating that `paprika()` is implemented in native code using JNI (Java Native Interface).

Now the last thing to do is to run `zero.apk` to see the content logged by Logcat.
This time I choose to plug my mobile phone via usb and install the app by [Android Debug Bridge](https://developer.android.com/studio/command-line/adb) utility...

```sh
$ adb install zero.apk
Performing Streamed Install
Success
$ adb logcat
...
```

Now that I've activated logcat to my device a lot of stuff is logged. To obtain the desidered log let's open the PicoCTF app installed before and type some a simple random string..on clicking the button let's see the adb printed logs..

```sh
...
02-06 17:51:09.198  2273  2273 I PICO    : picoCTF{a.moose.once.bit.my.sister}
...
```

Great, flag captured! :rocket:
