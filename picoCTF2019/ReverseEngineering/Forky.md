**In this [program](https://jupiter.challenges.picoctf.org/static/7bd9504475583513cd7a9f72ee79b106/vuln), identify the last integer value that is passed as parameter to the function doNothing().**

```sh
$ wget https://jupiter.challenges.picoctf.org/static/7bd9504475583513cd7a9f72ee79b106/vuln
$ file vuln
vuln: ELF 32-bit LSB pie executable, Intel 80386, version 1 (SYSV), dynamically linked, interpreter /lib/ld-linux.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=836c8d5ecaad6d64f4a358cf73d060d0c5050e87, not stripped
```
The binary of this challenge is a 32-bit executable. Let's open it with Ghidra.

```c
undefined4 main(void)
{
  int *piVar1;
  
  piVar1 = (int *)mmap((void *)0x0,4,3,0x21,-1,0);
  *piVar1 = 1000000000;
  fork();
  fork();
  fork();
  fork();
  *piVar1 = *piVar1 + 0x499602d2;
  doNothing(*piVar1);
  return 0;
}
```

The main function above spawns four processes. This means that `doNothing()` is executed multiple times and we have to find the last invocation of this function with `*piVar1` value. But how can we achieve this value?

The key concept is that a new child process forked run the subsequent code. In particular, in this program the `fork()` function is called recursively for each spawned process. Since there are four recoursive invocations we have a total of 2^4 processes.

Let's write a simple C program to exploit the flag..
```c
#include <stdio.h>
int main() {
	printf("picoCTF{%d}\n", 1000000000 + (0x499602d2 * 16));
	return 0;
}
```
...and see what we get.
```sh
$ gcc writeup.c -o writeup
writeup.c: In function ‘main’:
writeup.c:4:51: warning: integer overflow in expression of type ‘int’ results in ‘-1721750240’ [-Woverflow]
    4 |  printf("picoCTF{%d}\n", 1000000000 + (0x499602d2 * 16));
      |
$ ./writeup
picoCTF{-721750240}
```

Allright!!!
