**The name of the game is [speed](https://www.youtube.com/watch?v=8piqd2BWeGI). Are you quick enough to solve this problem and keep it above 50 mph? [need-for-speed](https://jupiter.challenges.picoctf.org/static/f9abc386dfb1309e687344783f208b20/need-for-speed).**

Before analyzing the source code let's have a quick look about it's execution.

```sh
$ ./need-for-speed
Keep this thing over 50 mph!
============================

Creating key...
Not fast enough. BOOM!
```
Uhm, what's is happening is a key generation process which is interrupted due to time constraints.

To understand what `need-for-speed` binary does is reasonable to open a Ghidra project and explore it's code in C flavor.
This is the `main()` function, which represents the entry point of our game.

```c
undefined8 main(void) {
  header();
  set_timer();
  get_key();
  print_flag();
  return 0;
}
```

Ok, here there's no evidence of the logic related to key generation, which is adviced by the challenge hint. So it reasonable to inspect `get_key()` function.
Here is the respective source code.

```c
void get_key(void) {
  puts("Creating key...");
  key = calculate_key();
  puts("Finished");
  return;
}
```

Let's go deeper looking at `calculate_key()`

```c
undefined4 calculate_key(void) {
  int local_c;
  
  local_c = -0x319e0722;
  do {
    local_c = local_c + -1;
  } while (local_c != -0x18cf0391);
  return 0xe730fc6f;
}
```

All right! We've found the heart of the challenge. There is a `local_c` variable initialized with -0x319e0722 and incremented by 1 in order to reach -0x18cf0391, which is far 416220048 numbers...very time consuming before returning `0xe730fc6f` to `key` variable.
To cut up with this `local_c` increase let's put a breakpoint and modify its value.

Let's debug it with gdb-[peda](https://github.com/longld/peda)!

```sh
$ gdb -q need-for-speed
gdb-peda$ b calculate_key
gdb-peda$ r
   .....
   0x5555554007f0 <decrypt_flag+134>:	ret    
   0x5555554007f1 <calculate_key>:	push   rbp
   0x5555554007f2 <calculate_key+1>:	mov    rbp,rsp
=> 0x5555554007f5 <calculate_key+4>:	mov    DWORD PTR [rbp-0x4],0xce61f8de
   0x5555554007fc <calculate_key+11>:	sub    DWORD PTR [rbp-0x4],0x1
   0x555555400800 <calculate_key+15>:	cmp    DWORD PTR [rbp-0x4],0xe730fc6f
   0x555555400807 <calculate_key+22>:	jne    0x5555554007fc <calculate_key+11>
   0x555555400809 <calculate_key+24>:	mov    eax,DWORD PTR [rbp-0x4]
[------------------------------------stack-------------------------------------]
0000| 0x7fffffffdcb0 --> 0x7fffffffdcc0 --> 0x7fffffffdce0 --> 0x0 
0008| 0x7fffffffdcb8 --> 0x555555400897 (<get_key+26>:	mov    DWORD PTR [rip+0x2007bf],eax        # 0x55555560105c <key>)
0016| 0x7fffffffdcc0 --> 0x7fffffffdce0 --> 0x0 
0024| 0x7fffffffdcc8 --> 0x555555400947 (<main+45>:	mov    eax,0x0)
0032| 0x7fffffffdcd0 --> 0x7fffffffddd8 --> 0x7fffffffe150 ("/home/andrea/Documenti/my-writeups/picoCTF/2019/ReverseEngineering/need-for-speed")
0040| 0x7fffffffdcd8 --> 0x100000000 
0048| 0x7fffffffdce0 --> 0x0 
0056| 0x7fffffffdce8 --> 0x7ffff7dfab25 (<__libc_start_main+213>:	mov    edi,eax)
[------------------------------------------------------------------------------]
Legend: code, data, rodata, value

Breakpoint 1, 0x00005555554007f5 in calculate_key ()
gdb-peda$ b *0x555555400800
gdb-peda$ c
....
gdb-peda$ x/w ($rbp-0x4)
0x7fffffffdcac:	0xce61f8dd                       ## we're gonna to change its value to break the while loop
gdb-peda$ set *0x7fffffffdcac = 0xe730fc6f
gdb-peda$ c
Continuing.
Finished
Printing flag:
PICOCTF{Good job keeping bus #190ca38b speeding along!}
[Inferior 1 (process 14522) exited normally]
Warning: not running
```

Oh yeah, flag captured :rocket:

Solution: `PICOCTF{Good job keeping bus #190ca38b speeding along!}`
