**Find the pass, get the flag. Check out this [file](https://jupiter.challenges.picoctf.org/static/b12c6d058c7f52eb1fd2015cfd291716/one.apk).**

As seen in the previous droid challenge, once downloaded the apk file we're gonna to decompile with this online [decompiler](https://www.decompiler.com/).
And now we are ready to look at the source code.
```sh
$ unzip -q one.apk.zip
$ ls
resources  sources  one.apk.zip
$ find sources -type f -name 'MainActivity.java'
sources/com/hellocmu/picoctf/MainActivity.java
```

To start our analysis one of the preferable files is the one related to the main activity. This is the source code:

```java
package com.hellocmu.picoctf;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    Button button;
    Context ctx;
    TextView text_bottom;
    EditText text_input;
    TextView text_top;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main);
        this.text_top = (TextView) findViewById(R.id.text_top);
        this.text_bottom = (TextView) findViewById(R.id.text_bottom);
        this.text_input = (EditText) findViewById(R.id.text_input);
        this.ctx = getApplicationContext();
        System.loadLibrary("hellojni");
        this.text_top.setText(R.string.hint);
    }

    public void buttonClick(View view) {
        this.text_bottom.setText(FlagstaffHill.getFlag(this.text_input.getText().toString(), this.ctx));
    }
}
```

Looking at the `onCreate()` method, the presence of `text_input` edit text means the point of interaction with the user and the `buttonClick()` method certainly
acquires the written text to display or not the flag. In particular this process is perfomed inside `FlagstaffHill.getFlag()` static method.
So let's see what this method does..

```java
package com.hellocmu.picoctf;

import android.content.Context;

public class FlagstaffHill {
    public static native String fenugreek(String str);

    public static String getFlag(String input, Context ctx) {
        if (input.equals(ctx.getString(R.string.password))) {
            return fenugreek(input);
        }
        return "NOPE";
    }
}
```

Very good, the flag appearance is affected by the equality between the string written by the user and the password resource string `R.string.password`.
In Android development all resources stuff like strings, xml content ecc is under the `res/` directory of the app project.
In particular the `strings.xml` collects all strings referenced in the code.

```sh
$ find resources -type f -name 'strings.xml'
...
resources/res/values/strings.xml
...
```

I've omitted to display the complete list of `strings.xml` occurrences due to compatibility with spoken languages for which the app is distributed. Our attention is focused only to the default `strings.xml`.

```xml
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <!-- some strings here -->
    <string name="password">opossum</string>
    <!-- other strings -->
</resources>
```

All right, the only thing left is to run the app with the apk

```sh
$ adb install one.apk
Performing Streamed Install
Success
```

..and this is what happens if I write "opossum"

![droids1](./droids1.jpeg)

Great, another flag captured! :fireworks: