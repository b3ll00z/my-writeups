**Can you reverse this [Windows Binary](https://jupiter.challenges.picoctf.org/static/0ef5d0d6d552cd5e0bd60c2adbddaa94/win-exec-1.exe)?**

Before exploring the source code let's give a quick look on what happens during the execution of this challenge binary.
```sh
>win-exec-1.exe
Input a number between 1 and 5 digits: 100
Initializing...
Enter the correct key to get the access codes: hello
Incorrect key. Try again.
```
Ok, so the program invites the user to input a number and then a key. To understand the logic behind the scenes an inspection with Ghidra may clarify..
This is the function where the user interaction happens

```c
void FUN_00408040(void) {
  int iVar1;
  FILE *_File;
  uint uVar2;
  undefined extraout_DL;
  undefined extraout_DL_00;
  undefined extraout_DL_01;
  undefined uVar3;
  undefined uVar4;
  void *in_stack_ffffff88;
  int local_74;
  char local_6c [100];
  uint local_8;
  
  local_8 = DAT_0047b174 ^ (uint)&stack0xfffffffc;
  thunk_FUN_004083e0((wchar_t *)s_Input_a_number_between_1_and_5_d_0047b06c);
  thunk_FUN_00408430((wchar_t *)&DAT_0047b094);
  local_74 = 1;
  while (9 < (int)in_stack_ffffff88) {
    local_74 = local_74 + 1;
    in_stack_ffffff88 = (void *)((int)in_stack_ffffff88 / 10);
  }
  if (local_74 < 6) {
    thunk_FUN_004083e0((wchar_t *)s_Initializing..._0047b0b4);
    thunk_FUN_00407ff0(in_stack_ffffff88,local_74);
    do {
      iVar1 = __fgetchar();
      uVar4 = SUB41(in_stack_ffffff88,0);
    } while (iVar1 != 10);
    thunk_FUN_004083e0((wchar_t *)s_Enter_the_correct_key_to_get_the_0047b0c8);
    _File = (FILE *)___acrt_iob_func(0);
    _fgets(local_6c,100,_File);
    uVar2 = thunk_FUN_00407f60(local_6c);
    if ((char)uVar2 == '\0') {
      thunk_FUN_004083e0((wchar_t *)s_Incorrect_key._Try_again._0047b0f8);
      uVar3 = extraout_DL_00;
    }
    else {
      thunk_FUN_004083e0((wchar_t *)s_Correct_input._Printing_flag:_0047b114);
      thunk_FUN_00408010();
      uVar3 = extraout_DL_01;
    }
  }
  else {
    thunk_FUN_004083e0((wchar_t *)s_Number_too_big._Try_again._0047b098);
    uVar4 = SUB41(in_stack_ffffff88,0);
    uVar3 = extraout_DL;
  }
  thunk_FUN_004084bf(local_8 ^ (uint)&stack0xfffffffc,uVar3,uVar4);
  return;
}
```
At first sight it seems that the foundamental part of this main function is the following piece of code:

```c
uVar2 = thunk_FUN_00407f60(local_6c);
```

where the value returned to `uVar2` is responsible for printing the flag content. To have an idea of what this function does let's have a quick look into it.

```c
uint __cdecl FUN_00407f60(char *param_1) {
  char *_Str;
  size_t sVar1;
  size_t sVar2;
  uint local_8;
  
  _Str = (char *)thunk_FUN_00407f40();
  local_8 = 0;
  while( true ) {
    sVar1 = _strlen(param_1);
    if (sVar1 - 1 <= local_8) {
      return CONCAT31((int3)(sVar1 - 1 >> 8),1);
    }
    sVar1 = _strlen(param_1);
    sVar2 = _strlen(_Str);
    if (sVar1 - 1 != sVar2) break;
    if (param_1[local_8] != _Str[local_8]) {
      return (uint)(_Str + local_8) & 0xffffff00;
    }
    local_8 = local_8 + 1;
  }
  return sVar2 & 0xffffff00;
}
```

Well, honestly there's no clear evidence of what this function exactly does. Anyway, inside the while loop there are two conditions able to break the loop.
Both conditions refer to a `_Str` variable given by `thunk_FUN_00407f40()`. Now I won't show you in details but after some deep inspection I've reached the conclusion that
there's no hardcoded string returned, which this means that `_Str` is initialized at runtime. For this reason a better understanding of its value may be shown with the bunch of assembly instructions referred.

```nasm
                                 *************************************************************************
                                 *                               FUNCTION                                *
                                 *************************************************************************
                                 uint __cdecl FUN_00407f60(char * param_1)
               uint                 EAX:4            <RETURN>
               char *               Stack[0x4]:4     param_1                                        XREF[3]:       00407f81(R), 
                                                                                                                   00407f95(R), 
                                                                                                                   00407fb6(R)  
               undefined4           Stack[-0x8]:4    local_8                                        XREF[6]:       00407f6f(W), 
                                                                                                                   00407f78(R), 
                                                                                                                   00407f7e(W), 
                                                                                                                   00407f90(R), 
                                                                                                                   00407fb9(R), 
                                                                                                                   00407fc2(R)  
               undefined4           Stack[-0xc]:4    local_c                                        XREF[3]:       00407f6c(W), 
                                                                                                                   00407fa6(R), 
                                                                                                                   00407fbf(R)  
                                 FUN_00407f60                                            XREF[1]:       thunk_FUN_00407f60:00402db5(T), 
                                                                                                         thunk_FUN_00407f60:00402db5(j)  
           00407f60 55                PUSH         EBP
           00407f61 8b ec             MOV          EBP,ESP
           00407f63 83 ec 08          SUB          ESP,0x8
           00407f66 56                PUSH         ESI
           00407f67 e8 7c c1 ff       CALL         thunk_FUN_00407f40                                       int thunk_FUN_00407f40(void)
                    ff
           00407f6c 89 45 f8          MOV          dword ptr [EBP + local_c],EAX
           00407f6f c7 45 fc 00       MOV          dword ptr [EBP + local_8],0x0
                    00 00 00
           00407f76 eb 09             JMP          LAB_00407f81
```
All right, so the idea is to put a breakpoint onto `00407f6c` and see the value returned to `EAX` register. To do this we've to use a proper Windows debugger.
For this scope I choose [OllyDbg](http://www.ollydbg.de/), as suggested by the challenge prologue. Once toggled the breakpoint at `00407f6c` the execution stops right here, with the layout below.

```sh
...
$-5   . E8 7CC1FFFF CALL win-exec.004040E8
$ ==> . 8945 F8     MOV DWORD PTR SS:[EBP-8],EAX
...

Registers (FPU)
EAX 006262C8 ASCII "The key is: 4253360"
...
```

OK, so now we know what is the expected string. To prove that is correct let's execute again our binary.

```sh
>win-exec-1.exe
Input a number between 1 and 5 digits: 100
Initializing...
Enter the correct key to get the access codes: The key is: 4253360
Correct input. Printing flag: PICOCTF{These are the access codes to the vault: 1063340}
```

Very good, flag captured :fire: