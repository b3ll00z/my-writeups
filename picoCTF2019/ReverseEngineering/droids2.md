**Find the pass, get the flag. Check out this [file](https://jupiter.challenges.picoctf.org/static/b7d30de6eaaf83e685aea7c10c5bdea8/two.apk).**

```sh
$ wget https://jupiter.challenges.picoctf.org/static/b7d30de6eaaf83e685aea7c10c5bdea8/two.apk
$ file two.apk
two.apk: Zip archive data, at least v1.0 to extract
```
This challenge is another one of Android reverse challs. We're going to extract the source with our [decompiler](https://www.decompiler.com/) friend seen previously.

```sh
$ unzip -q two.apk.zip
$ ls
resources sources two.apk.zip
$ find sources -type f -name 'MainActivity.java'
sources/com/hellocmu/picoctf/MainActivity.java
```

Same setup commands, now it's time to see the main activity source code.

```java
package com.hellocmu.picoctf;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    Button button;
    Context ctx;
    TextView text_bottom;
    EditText text_input;
    TextView text_top;

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView((int) R.layout.activity_main);
        this.text_top = (TextView) findViewById(R.id.text_top);
        this.text_bottom = (TextView) findViewById(R.id.text_bottom);
        this.text_input = (EditText) findViewById(R.id.text_input);
        this.ctx = getApplicationContext();
        System.loadLibrary("hellojni");
        this.text_top.setText(R.string.hint);
    }

    public void buttonClick(View view) {
        this.text_bottom.setText(FlagstaffHill.getFlag(this.text_input.getText().toString(), this.ctx));
    }
}
```

Again the main activity displays a button field which af a `buttonClick()` listener attached. A look into `FlagstaffHill` could give a detailed information about the flag. This is its source code.

```java
package com.hellocmu.picoctf;

import android.content.Context;

public class FlagstaffHill {
    public static native String sesame(String str);

    public static String getFlag(String input, Context ctx) {
        String[] witches = {"weatherwax", "ogg", "garlick", "nitt", "aching", "dismass"};
        int second = 3 - 3;
        int third = (3 / 3) + second;
        int fourth = (third + third) - second;
        int fifth = 3 + fourth;
        if (input.equals("".concat(witches[fifth]).concat(".").concat(witches[third]).concat(".").concat(witches[second]).concat(".").concat(witches[(fifth + second) - third]).concat(".").concat(witches[3]).concat(".").concat(witches[fourth]))) {
            return sesame(input);
        }
        return "NOPE";
    }
}
```

The `getFlag()` method returns our flag, depending on the equality of `input` parameter, which comes from the user text submitted in main activity, and a long string given playing with `witches` array of strings. To save time I've prepared this Java program.

```java
public class FlagExtractor {
    public static void main(String[] args) {
        String[] witches = {"weatherwax", "ogg", "garlick", "nitt", "aching", "dismass"};
        int second = 3 - 3;
        int third = (3 / 3) + second;
        int fourth = (third + third) - second;
        int fifth = 3 + fourth;
        System.out.println("".concat(witches[fifth]).concat(".")
                             .concat(witches[third]).concat(".")
                             .concat(witches[second]).concat(".")
                             .concat(witches[(fifth + second) - third]).concat(".")
                             .concat(witches[3]).concat(".")
                             .concat(witches[fourth]));
    }
}
```

Now let's see what is the expected string..

```sh
$ javac FlagExtractor.java
$ java FlagExtractor
dismass.ogg.weatherwax.aching.nitt.garlick
```

Ok, now last thing to do is to plug my device via usb and install it `two.apk` with adb.
Once opened the app I got this message after typing the expected input string..

![droids2](./droids2.jpeg)

..and that's all folks!!! :smile: