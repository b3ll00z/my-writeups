**Cryptography can be easy, do you know what ROT13 is? `cvpbPGS{arkg_gvzr_V'yy_gel_2_ebhaqf_bs_ebg13_nSkgmDJE}`**

A useful command is `tr`, which can translate or delete characthers. [ROT-13](https://en.wikipedia.org/wiki/ROT13) is an ancient substitution cipher, where each letter is swapped with another one 13 positions ahead. 
So we can use `tr` with the purpose to translate the cipher as below:

```
$ echo "cvpbPGS{arkg_gvzr_V'yy_gel_2_ebhaqf_bs_ebg13_nSkgmDJE}" | tr "N-ZA-Mn-za-m" "A-Za-z"
picoCTF{next_time_I'll_try_2_rounds_of_rot13_aFxtzQWR}
```
