**In RSA, a small ``e`` value can be problematic, but what about ``N``? Can you decrypt this? [values](https://mercury.picoctf.net/static/12d820e355a7775a2c9129b2622a7eb6/values)**

Here are our challenge values:
```sh
$ cat values
Decrypt my super sick RSA:
c: 843044897663847841476319711639772861390329326681532977209935413827620909782846667
n: 1422450808944701344261903748621562998784243662042303391362692043823716783771691667
e: 65537
```

According to this [guide](https://www.boxentriq.com/code-breaking/rsa) the decryption process to calcolate the plaintext `p` is `p = c^d mod n`.
The `d` exponent is the private key, which is calculated from the chosen prime numbers `p` and `q` and the public key `e` in order to solve the following equation:

```
de = 1 mod (p - 1)(q - 1)
```

which is really difficult to extract since with don't know `p` and `q`. However, giving the advice that the values on our knowledge make rsa encryption weak we could try with the powerful [RsaCftTool](https://github.com/Ganapati/RsaCtfTool)..

```sh
$ python RsaCtfTool.py -n 1422450808944701344261903748621562998784243662042303391362692043823716783771691667 -e 65537 --uncipher 843044897663847841476319711639772861390329326681532977209935413827620909782846667
private argument is not set, the private key will not be displayed, even if recovered.

[*] Testing key /tmp/tmph8xk1w90.
[*] Performing system_primes_gcd attack on /tmp/tmph8xk1w90.
[*] Performing smallq attack on /tmp/tmph8xk1w90.
[*] Performing pastctfprimes attack on /tmp/tmph8xk1w90.
[*] Performing mersenne_primes attack on /tmp/tmph8xk1w90.
[*] Performing fibonacci_gcd attack on /tmp/tmph8xk1w90.
[*] Performing factordb attack on /tmp/tmph8xk1w90.
[*] Attack success with factordb method !

Results for /tmp/tmph8xk1w90:

Unciphered data :
HEX : 0x007069636f4354467b736d6131315f4e5f6e305f67306f645f30303236343537307d
INT (big endian) : 13016382529449106065927291425342535437996222135352905256639555294957886055592061
INT (little endian) : 3710929847087427876431838308943291274263296323136963202115989746100135819907526656
utf-8 : picoCTF{sma11_N_n0_g0od_00264570}
utf-16 : 瀀捩䍯䙔獻慭ㄱ也湟弰で摯た㈰㐶㜵細
STR : b'\x00picoCTF{sma11_N_n0_g0od_00264570}'
```

There we gooo :fireworks:
