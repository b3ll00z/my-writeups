**Using tabcomplete in the Terminal will add years to your life, esp. when dealing with long rambling directory structures and filenames: [Addadshashanammu.zip](https://mercury.picoctf.net/static/e38f6a5b69b45d21e33cf7281d8c2531/Addadshashanammu.zip)**

```
$ unzip -q Addadshashanammu.zip
$ ls 
Addadshashanammu  Addadshashanammu.zip
$ tree Addadshashanammu
Addadshashanammu
└── Almurbalarammi
    └── Ashalmimilkala
        └── Assurnabitashpi
            └── Maelkashishi
                └── Onnissiralis
                    └── Ularradallaku
                        └── fang-of-haynekhtnamet

6 directories, 1 file
$ find . -type f -name 'fang*'
./Addadshashanammu/Almurbalarammi/Ashalmimilkala/Assurnabitashpi/Maelkashishi/Onnissiralis/Ularradallaku/fang-of-haynekhtnamet
```

Let's see what kind of file we're talking about...
```
$ file ./Addadshashanammu/Almurbalarammi/Ashalmimilkala/Assurnabitashpi/Maelkashishi/Onnissiralis/Ularradallaku/fang-of-haynekhtnamet
./Addadshashanammu/Almurbalarammi/Ashalmimilkala/Assurnabitashpi/Maelkashishi/Onnissiralis/Ularradallaku/fang-of-haynekhtnamet: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, for GNU/Linux 3.2.0, BuildID[sha1]=55548d0314fdf7999b966728d19712cdf8a52e58, not stripped
```

Ok, so a `strings` command is just all we need

```
$ strings ./Addadshashanammu/Almurbalarammi/Ashalmimilkala/Assurnabitashpi/Maelkashishi/Onnissiralis/Ularradallaku/fang-of-haynekhtnamet | grep 'pico'
*ZAP!* picoCTF{l3v3l_up!_t4k3_4_r35t!_f3553887}
```
Perfect! :rocket:
