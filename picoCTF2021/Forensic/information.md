**Files can always be changed in a secret way. Can you find the flag? [cat.jpg](https://mercury.picoctf.net/static/c28a959c5605d5f67480d5dd3a77f302/cat.jpg)**

The hint of this challenge suggests to look at the details of the file.

Let's upload it on this [analyzer](http://fotoforensics.com/). Now that the image is scanned there's a bunch of metadata information.
One thing that captures my attention is on *Licence* box, where a the content isn't a plain text, which I expected to see.
I think is a base64 encoded string. Let's prove it!

```
$ echo "cGljb0NURnt0aGVfbTN0YWRhdGFfMXNfbW9kaWZpZWR9" >> license.txt
$ base64 --decode license.txt
picoCTF{the_m3tadata_1s_modified}
```

Allright, flag captured!
