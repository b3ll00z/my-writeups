**Matryoshka dolls are a set of wooden dolls of decreasing size placed one inside another. What's the final one? Image: [this](https://mercury.picoctf.net/static/205adad23bf9d8303081a0e71c9beab8/dolls.jpg)**

So we have an image which recursively contains another image, like a matryoshka doll. To extract all these encapsulated 
images I suggest to use [binwalk](https://github.com/ReFirmLabs/binwalk), which is a tool for searching binary files like images and audio files for embedded files and data.

Looking at its help page there are a lot of extraction options.

```
$ binwalk --help
...

Extraction Options:
    -e, --extract                Automatically extract known file types
    -D, --dd=<type:ext:cmd>      Extract <type> signatures, give the files an extension of <ext>, and execute <cmd>
    -M, --matryoshka             Recursively scan extracted files
    -d, --depth=<int>            Limit matryoshka recursion depth (default: 8 levels deep)
    -C, --directory=<str>        Extract files/folders to a custom directory (default: current working directory)
    -j, --size=<int>             Limit the size of each extracted file
    -n, --count=<int>            Limit the number of extracted files
    -r, --rm                     Delete carved files after extraction
    -z, --carve                  Carve data from files, but don't execute extraction utilities
    -V, --subdirs                Extract into sub-directories named by the offset
...
```

For the purpose of this challenge we're going surely to an automatic extraction with a recursively scan, like `--matryoshka` states.

```
$ binwalk -eM dolls.jpg
...
$ tree _dolls.jpg.extracted
_dolls.jpg.extracted
├── 4286C.zip
└── base_images
    ├── 2_c.jpg
    └── _2_c.jpg.extracted
        ├── 2DD3B.zip
        └── base_images
            ├── 3_c.jpg
            └── _3_c.jpg.extracted
                ├── 1E2D6.zip
                └── base_images
                    ├── 4_c.jpg
                    └── _4_c.jpg.extracted
                        ├── 136DA.zip
                        └── flag.txt

6 directories, 8 files
```

Ooh yes, there we go!!!

```
$ cat ./_dolls.jpg.extracted/base_images/_2_c.jpg.extracted/base_images/_3_c.jpg.extracted/base_images/_4_c.jpg.extracted/flag.txt
picoCTF{96fac089316e094d41ea046900197662}
```
