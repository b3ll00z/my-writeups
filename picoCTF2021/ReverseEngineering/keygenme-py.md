**[https://mercury.picoctf.net/static/fb75b48f9214cf992a2199b5785564e7/keygenme-trial.py](https://mercury.picoctf.net/static/fb75b48f9214cf992a2199b5785564e7/keygenme-trial.py)**

After an inspection on source code the following function catched my attention.

```python
def check_key(key, username_trial):

    global key_full_template_trial

    if len(key) != len(key_full_template_trial):
        return False
    else:
        # Check static base key part --v
        i = 0
        for c in key_part_static1_trial:
            if key[i] != c:
                return False

            i += 1

        # TODO : test performance on toolbox container
        # Check dynamic part --v
        if key[i] != hashlib.sha256(username_trial).hexdigest()[4]:
            return False
        else:
            i += 1

        if key[i] != hashlib.sha256(username_trial).hexdigest()[5]:
            return False
        else:
            i += 1

        if key[i] != hashlib.sha256(username_trial).hexdigest()[3]:
            return False
        else:
            i += 1

        if key[i] != hashlib.sha256(username_trial).hexdigest()[6]:
            return False
        else:
            i += 1

        if key[i] != hashlib.sha256(username_trial).hexdigest()[2]:
            return False
        else:
            i += 1

        if key[i] != hashlib.sha256(username_trial).hexdigest()[7]:
            return False
        else:
            i += 1

        if key[i] != hashlib.sha256(username_trial).hexdigest()[1]:
            return False
        else:
            i += 1

        if key[i] != hashlib.sha256(username_trial).hexdigest()[8]:
            return False
            
        return True
```        

Going on order, the first check regards the first static part of key, which is reported in the declaration `key_part_static1_trial = "picoCTF{1n_7h3_|<3y_of_"`.
So the code expect this value as first part of licence key to provide. The following part is reported in the sequence of if statements checking each single character with a specific character of `username_trial` hexdigested by sha256.
Since `username_trial` is initialized with `"FREEMAN"` string we can save time printing the hashed characters in the order represented above by these if statements.
I've prepared this simple program..

```python
import hashlib
key_part1 = "picoCTF{1n_7h3_|<3y_of_"
key_part2 = "}"
username_trial = b"FREEMAN"
hashed_username = hashlib.sha256(username_trial).hexdigest()
print("The licence key is: {0}{1}{2}".format(key_part1, "".join(hashed_username[i] for i in [4, 5, 3, 6, 2, 7, 1, 8]), key_part2))
```
..that gives the following output:

```sh
$ python keygen_crack.py
The licence key is: picoCTF{1n_7h3_|<3y_of_0d208392}
```

All right!!
