**I wonder what this really is... [enc](https://mercury.picoctf.net/static/e47483f88b12f2ab0c46315afc12f64d/enc) `''.join([chr((ord(flag[i]) << 8) + ord(flag[i + 1])) for i in range(0, len(flag), 2)])`**

Once retrieved the enc file this is what we see,

```
$ cat enc
灩捯䍔䙻ㄶ形楴獟楮獴㌴摟潦弸彥ㄴㅡて㝽
```

So we have a bunch of (I suppose) chinese characters which must be processed to be manipulated as numbers.
To save time and maybe attempts a powerful tool like [CyberCheg](https://github.com/gchq/CyberChef) comes in help. It's an extremely useful web app with many features about text processing and encoding techniques.
The idea is to paste directly the chinese string and see with just an in-depth investigation if a flag could be disclosed.
[Here](https://gchq.github.io/CyberChef/#recipe=Magic(3,true,false,'')&input=54Gp5o2v5I2U5Jm744S25b2i5qW0542f5qWu542044y05pGf5r2m5by45b2l44S044Wh44Gm4529) there are many results given by this analysis. As you can see the same string reported in UTF-16BE encoding has the content `picoCTF{16_bits_inst34d_of_8_e141a0f7}`.
