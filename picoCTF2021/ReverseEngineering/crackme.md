**[crackme.py](https://mercury.picoctf.net/static/2ff6c888060f14af5db1232e319547c9/crackme.py)**

In this challenge there's no prohibition explicited to modify the source code of the program.
So the easiest way to solve is to patch the program with these last two lines:

```python
decode_secret(bezos_cc_secret)
#choose_greatest()
```

Now let's see what's happen..

```sh
$ python crackme
picoCTF{1|\/|_4_p34|\|ut_ef5b69a3}
```

Great! :fire:
