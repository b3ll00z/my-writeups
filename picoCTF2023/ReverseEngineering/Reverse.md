**Try reversing this file? Can ya?
I forgot the password to this [file](https://artifacts.picoctf.net/c/246/ret). Please find it for me?**

```
$ file ret
ret: ELF 64-bit LSB pie executable, x86-64, version 1 (SYSV), dynamically linked, interpreter /lib64/ld-linux-x86-64.so.2, BuildID[sha1]=aea3d7e93200a53390a7a88a38ebf96c06062daa, for GNU/Linux 3.2.0, not stripped
```
Before executing this binary it could be a good idea to simply decompile with a proper tool like [Ghidra](https://ghidra-sre.org/). I found this source code as the implementation of `main()`.

```c
undefined8 main(void)

{
  int iVar1;
  long in_FS_OFFSET;
  char local_68 [48];
  undefined8 local_38;
  undefined8 local_30;
  undefined8 local_28;
  undefined8 local_20;
  undefined8 local_18;
  long local_10;
  
  local_10 = *(long *)(in_FS_OFFSET + 0x28);
  local_38 = 0x7b4654436f636970;
  local_30 = 0x337633725f666c33;
  local_28 = 0x75735f676e693572;
  local_20 = 0x6c75663535656363;
  local_18 = 0x316339343963335f;
  printf("Enter the password to unlock this file: ");
  __isoc99_scanf(&DAT_00102031,local_68);
  printf("You entered: %s\n",local_68);
  iVar1 = strcmp(local_68,(char *)&local_38);
  if (iVar1 == 0) {
    puts("Password correct, please see flag: picoCTF{3lf_r3v3r5ing_succe55ful_3c949c1b}");
    puts((char *)&local_38);
  }
  else {
    puts("Access denied");
  }
  if (local_10 != *(long *)(in_FS_OFFSET + 0x28)) {
                    /* WARNING: Subroutine does not return */
    __stack_chk_fail();
  }
  return 0;
}
```

Well, this time we haven't spent a lot to find the flag :stuck_out_tongue_winking_eye:
