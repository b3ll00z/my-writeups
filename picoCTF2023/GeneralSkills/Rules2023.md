**Read the rules of the competition and get a little bonus!
[Rules](https://picoctf.org/competitions/2023-spring-rules.html)**

After some search here's is the main paragraph..

```
...
What are the other rules and terms for picoCTF 2023?
CMU may display one or more Competition leaderboards and/or scoreboards. These boards may be available to other Participants and/or the public, and may show the Team name, the country of residence and/or school identified by the Team leader, and/or the name of the School identified by the Team leader. picoCTF{h34rd_und3r5700d_4ck_cba1c711}
...
```
