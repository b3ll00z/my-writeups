**Can you make sense of this file?
Download the file [here](https://artifacts.picoctf.net/c/302/enc_flag).**

```
$ wget https://artifacts.picoctf.net/c/302/enc_flag
$ cat enc_flag
VmpGU1EyRXlUWGxTYmxKVVYwZFNWbGxyV21GV1JteDBUbFpPYWxKdFVsaFpWVlUxWVZaS1ZWWnVh
RmRXZWtab1dWWmtSMk5yTlZWWApiVVpUVm10d1VWZFdVa2RpYlZaWFZtNVdVZ3BpU0VKeldWUkNk
MlZXVlhoWGJYQk9VbFJXU0ZkcVRuTldaM0JZVWpGS2VWWkdaSGRXCk1sWnpWV3hhVm1KRk5XOVVW
VkpEVGxaYVdFMVhSbFZrTTBKVVZXcEJlRTVzV2tkYVNHUlZDazFyY0ZoWlZFNXpWa2RHZEdWRlZs
aGkKYlRrelZERldUMkpzUWxWTlJYTkxDZz09Cg==
```

So this encoded message reminds me that the flag could be hidden by a base64 encoding several times.
To speed up our flag research the idea is to write the following program that works for us

```python
import os
import base64

base64_nested_flag_file = open(os.getcwd() + '/enc_flag')
base64_nested_flag = base64_nested_flag_file.read()

while True:
	base64_nested_flag = base64.b64decode(base64_nested_flag).decode('ascii')
	print(base64_nested_flag)
	if base64_nested_flag.startswith('picoCTF{'):
		break

base64_nested_flag_file.close()
```
And let's launch the script..
```
$ python extract_b64_flag.py
VjFSQ2EyTXlSblJUV0dSVllrWmFWRmx0TlZOalJtUlhZVVU1YVZKVVZuaFdWekZoWVZkR2NrNVVX
bUZTVmtwUVdWUkdibVZXVm5WUgpiSEJzWVRCd2VWVXhXbXBOUlRWSFdqTnNWZ3BYUjFKeVZGZHdW
MlZzVWxaVmJFNW9UVVJDTlZaWE1XRlVkM0JUVWpBeE5sWkdaSGRVCk1rcFhZVE5zVkdGdGVFVlhi
bTkzVDFWT2JsQlVNRXNLCg==

V1RCa2MyRnRTWGRVYkZaVFltNVNjRmRXYUU5aVJUVnhWVzFhYVdGck5UWmFSVkpQWVRGbmVWVnVR
bHBsYTBweVUxWmpNRTVHWjNsVgpXR1JyVFdwV2VsUlZVbE5oTURCNVZXMWFUd3BTUjAxNlZGZHdU
MkpXYTNsVGFteEVXbm93T1VOblBUMEsK

WTBkc2FtSXdUbFZTYm5ScFdWaE9iRTVxVW1aaWFrNTZaRVJPYTFneVVuQlpla0pyU1ZjME5GZ3lV
WGRrTWpWelRVUlNhMDB5VW1aTwpSR016VFdwT2JWa3lTamxEWnowOUNnPT0K

Y0dsamIwTlVSbnRpWVhObE5qUmZiak56ZEROa1gyUnBZekJrSVc0NFgyUXdkMjVzTURSa00yUmZO
RGMzTWpObVkySjlDZz09Cg==

cGljb0NURntiYXNlNjRfbjNzdDNkX2RpYzBkIW44X2Qwd25sMDRkM2RfNDc3MjNmY2J9Cg==

picoCTF{base64_n3st3d_dic0d!n8_d0wnl04d3d_47723fcb}
```
Great!
