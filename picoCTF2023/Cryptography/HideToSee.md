**How about some hide and seek heh?
Look at this image [here](https://artifacts.picoctf.net/c/438/atbash.jpg).**

```
$ wget https://artifacts.picoctf.net/c/438/atbash.jpg
$ file atbash.jpg
atbash.jpg: JPEG image data, JFIF standard 1.01, aspect ratio, density 1x1, segment length 16, baseline, precision 8, 465x455, components 3
```

Once inspected the image there's a reference about the [Ashbat cipher](https://en.wikipedia.org/wiki/Atbash), which an historical monoalphabetic substitution cipher.
The hint of this challenge suggests to extract data form this file, so let's try with [steghide](https://steghide.sourceforge.net/documentation.php) tool.
```
$ steghide extract -sf atbash.jpg                                                         
Enter passphrase: 
wrote extracted data to "encrypted.txt".
$ cat encrypted.txt
krxlXGU{zgyzhs_xizxp_01vy23wu}
```
Nice, so we're near to discover the hidden flag. The solution is to simply use an atbash decoder, which can be found [here](https://www.boxentriq.com/code-breaking/atbash-cipher).
Once submitted the ciphertext we get this plaintext `picoCTF{atbash_crack_01eb23df}`.
Nice :smile:
