**Every file gets a flag.
The SOC analyst saw one image been sent back and forth between two people. They decided to investigate and found out that there was more than what meets the eye [here](https://artifacts.picoctf.net/c/315/flag.png).**

```
$ wget https://artifacts.picoctf.net/c/315/flag.png
$ file flag.png
flag.png: PNG image data, 512 x 504, 8-bit/color RGBA, non-interlaced
```

Ok, we got a plain png image. Since this is a forensic challenge the idea to solve it could be trying to extract some stuff from this png.
Let's check it out with the powerful [binwalk](https://github.com/ReFirmLabs/binwalk)..

```
$ binwalk -eM flag.png
$ tree _flang.png.extracted
_flag.png.extracted
├── 29
├── 29.zlib
├── 9B3B.zip
└── secret
    ├── flag.png
    └── _flag.png.extracted
        ├── 86
        └── 86.zlib

3 directories, 6 files
```
Well, effectively it seems to have another embedded image at  _flag.png.extracted/secret/flag.png. 
![hideme-flag](./hideme-flag.png)
