**This image passes LSB statistical analysis, but we can't help but think there must be something to the visual artifacts present in this image... Download the image [here](https://artifacts.picoctf.net/c/303/Ninja-and-Prince-Genji-Ukiyoe-Utagawa-Kunisada.flag.png)**

```
$ file Ninja-and-Prince-Genji-Ukiyoe-Utagawa-Kunisada.flag.png
Ninja-and-Prince-Genji-Ukiyoe-Utagawa-Kunisada.flag.png: PNG image data, 1074 x 1500, 8-bit/color RGB, non-interlaced
$ pngcheck Ninja-and-Prince-Genji-Ukiyoe-Utagawa-Kunisada.flag.png
OK: Ninja-and-Prince-Genji-Ukiyoe-Utagawa-Kunisada.flag.png (1074x1500, 24-bit RGB, non-interlaced, 30.6%).
$ binwalk -Em Ninja-and-Prince-Genji-Ukiyoe-Utagawa-Kunisada.flag.png
...
$ tree extractions
extractions
└── Ninja-and-Prince-Genji-Ukiyoe-Utagawa-Kunisada.flag.png -> /tmp/Ninja-and-Prince-Genji-Ukiyoe-Utagawa-Kunisada.flag.png
```

At first sight there's no evidence of something strange inside the image nor inside its png format.
Being a forensic challenge the idea is to still perform checks on the image. A useful information is the fact that this file passes the LSB statistical analysis. According with this [paper](https://ijcst.com/vol33/4/anil2.pdf) in steganography LSB is a method to hide secret informations using the least significant bit of the image pixels. Instead, the MSB manage the same goal by the most significant bit.

So this challange focuses on MSB technique which can be used to decode something from this corrupted png.
After googling something I found this useful python LSB/MSB decoder https://github.com/Pulho/sigBits... let's try!

```
$ python sigBits.py --type=MSB Ninja-and-Prince-Genji-Ukiyoe-Utagawa-Kunisada.flag.png
Done, check the output file!
```

The output file is `outputSB.txt`, which contains a lot of text inside. An inspection with vim may find the flag..

```
    an an apple.""If that be so," said Panza, "I renounce henceforth the government ofthe promised island, and desire nothing more in payment of my many andfaithful services     than that your worship give me the receipt of thissupreme liquor, for I am persuaded it will be worth more than two realsan ounce anywhere, and I want no more to pass t    he rest of my life inease and honour; but it remains to be told if it costs much to makeit.""With less than three reals, six quarts of it may be made," said DonQuixote."    Sinner that I am!" said Sancho, "then why does your worship put offmaking it and teaching it to me?""Peace, friend," answered Don Quixote; "greater secrets I mean to tea    chthee and greater favours to bestow upon thee; and for the present letus see to the dressing, for my ear pains me more than I could wish."Sancho took out some lint and     ointment from the alforjas; but when DonQuixote came to see his helmet shattered, he was like to lose hissenses, and clapping his hand upon his sword and raising his eye    s toheaven, he said, "I swear by the Creator of all things and the fourGospels in their fullest extent, to do as the great Marquis of Mantuadid when he swore to avenge t    he death of his nephew Baldwin (and thatwas not to eat bread from a table-cloth, nor embrace his wife, andother points which, though I cannot now call them to mind, I he    re grantas expressed) until I take complete vengeance upon him who hascommitted such an offence against me."Hearing this, Sancho said to him, "Your worship should bear i    n mind,Senor Don Quixote, that if the knight has done what was commanded himin going to present himself before my lady Dulcinea del Toboso, he willhave done all that he     was bound to do, and does not deserve furtherpunishment unless he commits some new offence."picoCTF{15_y0ur_que57_qu1x071c_0r_h3r01c_24d55bee}"Thou hast said well and hi
/pico
```
