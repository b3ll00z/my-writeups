**Ron just found his own copy of advanced potion making, but its been corrupted by some kind of spell. Help him recover it!**

```
$ wget https://artifacts.picoctf.net/picoMini+by+redpwn/Forensics/advanced-potion-making/advanced-potion-making
$ file advanced-potion-making
advanced-potion-making: data
```

Ok, there's no immediate evidence about the nature of this file. The challenge suggests the file may be a corrupted image. Let's try to inspect with a png checker.

```
$ pngcheck -v advanced-potion-making                                                            2 ✘ 
File: advanced-potion-making (30372 bytes)
  this is neither a PNG or JNG image nor a MNG stream
$ xxd -g1 advanced-potion-making | less
00000000: 89 50 42 11 0d 0a 1a 0a 00 12 13 14 49 48 44 52  .PB.........IHDR
00000010: 00 00 09 90 00 00 04 d8 08 02 00 00 00 04 2d e7  ..............-.
00000020: 78 00 00 00 01 73 52 47 42 00 ae ce 1c e9 00 00  x....sRGB.......
00000030: 00 04 67 41 4d 41 00 00 b1 8f 0b fc 61 05 00 00  ..gAMA......a...
00000040: 00 09 70 48 59 73 00 00 16 25 00 00 16 25 01 49  ..pHYs...%...%.I
00000050: 52 24 f0 00 00 76 39 49 44 41 54 78 5e ec fd 61  R$...v9IDATx^..a
00000060: 72 e3 4c 94 a6 59 ce 16 6a fe 76 cd fe 57 d7 dd  r.L..Y..j.v..W..
00000070: 5b 18 45 e9 4b 8a 7a 28 d1 9d 20 48 07 a9 63 76  [.E.K.z(.. H..cv
...
```

Well, after some research I suppose that this file is just a corrupted png. This [link](https://asecuritysite.com/forensics/png?file=%2Flog%2Fbasn0g01.png) shows that
the first 16 bytes must be compliant to the png header, which refers to this sequence `89 50 4E 47 0D 0A 1A 0A 00 00 00 0D 49 48 44 52`.
So, the idea is to write manually these required bytes inside this challenge file using vim editor:.

```
   1 00000000: 8950 4e47 0d0a 1a0a 0000 000d 4948 4452  .PB.........IHDR
   2 00000010: 0000 0990 0000 04d8 0802 0000 0004 2de7  ..............-.
   3 00000020: 7800 0000 0173 5247 4200 aece 1ce9 0000  x....sRGB.......
   4 00000030: 0004 6741 4d41 0000 b18f 0bfc 6105 0000  ..gAMA......a...
   5 00000040: 0009 7048 5973 0000 1625 0000 1625 0149  ..pHYs...%...%.I
:%!xxd -r
```

Now let's check if the image is effectively recovered.
```
$ mv advanced-potion-making advanced-potion-making.png
$ pngcheck -v advanced-potion-making.png
File: advanced-potion-making.png (30373 bytes)
  chunk IHDR at offset 0x0000c, length 13
    2448 x 1240 image, 24-bit RGB, non-interlaced
  chunk sRGB at offset 0x00025, length 1
    rendering intent = perceptual
  chunk gAMA at offset 0x00032, length 4: 0.45455
  chunk pHYs at offset 0x00042, length 9: 5669x5669 pixels/meter (144 dpi)
  chunk IDAT at offset 0x00057, length 30265
    zlib: deflated, 32K window, fast compression
  chunk IEND at offset 0x0769c, length 0
  additional data after IEND chunk
ERRORS DETECTED in advanced-potion-making.png
```
If I try to open this image with a viewer a total red background is shown. Probably this image contains hidden layers or something similar.
An idea is to inspect this recovered png with [`stegsolve`](https://github.com/zardus/ctf-tools/blob/master/stegsolve/install) tool.
Inspecting some views there's something really interesting...
![advanced-potion-making-flag](./advanced-potion-making-flag.png)
Great!!
